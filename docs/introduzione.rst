Introduzione
============

Il sistema NLGest, per la **gestione delle Newsletter di Infoparlamento**,
è un\'*applicazione web* multi-utente, che permette ai diversi *attori* di definire ed inviare
delle newsletter a gruppi di clienti.

Allo stesso tempo il sistema costituisce un *archivio* dove i clienti possono visualizzare le newsletter inviate.

Il sistema si compone di due *interfacce*:

- un interfaccia di gestione (o *backend*), dove i redattori o l'amministratore definiscono i contenuti delle newsletter,
  i temi che esse trattano, le notizie di cui si compongono, e i destinatari, e che permette l'invio della
  singola newsletter;
- un interfaccia di visualizzazione (o *frontend*) destinata ai clienti, ad accesso protetto da username e password, che permette la
  visualizzazione delle singole notizie, partendo dai link ipertestuali contenuti nelle email, e l'esplorazione
  dell'archivio delle newsletter inviate in passato.
