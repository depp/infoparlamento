Manuale utente per amministratori
=================================

Una volta effettuato il login, i redattori visualizzano queste sezioni dell'interfaccia di
amministrazione:

.. image:: images/amministratori_home.png
  :width: 760
  :alt: Interfaccia backend - Home page amministratori

Gli amministratori possono eseguire tutte le operazioni svolte dai redattori, più altre operazioni tra cui:

- gestione dei clienti
- gestione delle utenze
- gestione delle newsletter
- set delle notizie pronte per l'invio
- gestione delle categorie
- gestione dell'ordinamento delle categorie di una particolare newsletter
- gestione e invio delle newsletter

Gestione clienti
----------------
Cliccando sul pulsante **Clienti** viene mostrato l'elenco dei clienti inseriti fino ad ora.

.. image:: images/amministratori_clienti.png
  :width: 760
  :alt: Interfaccia backend - elenco clienti

È possibile filtrare l'elenco dei clienti, usando i filtri nella zona a destra. I filtri possibili sono:

- per status di attività (vero/falso) - lo status indica la condizione di validità del contratto
- per status Vuole tutto (vero/falso) - lo status indica se il cliente vuole ricevere tutte le news,
  indipendentemente dall'argomento (Tag) con cui sono state categorizzate

Utilizzando il motore di ricerca posto in cima all'elenco dei clienti, è possibile effettuare delle ricerche
in questi campi:

- nome del cliente
- indirizzo email ufficiale di un cliente
- nome del contatto referente per un cliente
- indirizzo email del contatto per un cliente

Cliccando sul pulsante **Aggiungi cliente**, si passa al modulo di aggiunta di un nuovo cliente:

.. image:: images/amministratori_cliente_nuovo.png
  :width: 760
  :alt: Interfaccia backend - aggiunta nuovo cliente

I campi in neretto sono obbligatori.

Cliccando sul link di un cliente, si passa alla pagina di modifica di un cliente.

.. image:: images/amministratori_cliente_modifica.png
  :width: 760
  :alt: Interfaccia backend - modifica di un cliente

Per un cliente, i dati anagrafici sono:

- Nome - la denominazione ufficiale del cliente
- Partita IVA - la partita IVA, utilizzata in sede di fatturazione
- Codice univoco ... - il codice per la fatturazione elettronica
- Indirizzo - l'indirizzo della sede legale
- Telefono - il telefono ufficiale
- Indirizzo email ufficiale
- Nome referente - il nome del contatto referente
- Telefono referente
- Indirizzo email referente
- Data di inizio contratto
- Data di fine contratto
- È attivo - Flag di stato attività
- Vuole tutto - Flag che indica se l'utente vuole ricevere news di ogni tipo,
  indipendentemente dagli argomenti (tag) con cui queste sono state categorizzate

Il controllo sull'invio è fatto esclusivamente sul flag di stato attività e le date di inizio e
fine del contratto sono indicative.

Gestione tag
++++++++++++

Oltre ai dati anagrafici, è possibile aggiungere o rimuovere gli argomenti (i Tag) ai quali il cliente
è interessato, nella sezione *Relazioni con i tag*

.. image:: images/amministratori_cliente_tag.png
  :width: 760
  :alt: Interfaccia backend - modifica dei tag per un cliente

Ogni volta che a un cliente viene aggiunto o rimosso un tag, viene automaticamente effettuato
il ricalcolo dei clienti destinatari per tutte le notizie coinvolte nella modifica, in base all'algoritmo di matching.

L'elenco dei clienti destinatari è mostrato nella sezione **Notizie che riceveranno con il prossimo invio** della stessa pagina di gestione della
notizia (vedi più avanti).

Attraverso il pulsante **Aggiungi un altro Relazione con Tag** è possibile aggiungere altri tag.

Gestione destinatari del cliente
++++++++++++++++++++++++++++++++

A un cliente possono essere associati diversi indirizzi email, cui saranno spedite le email.

.. image:: images/amministratori_cliente_destinatari.png
  :width: 760
  :alt: Interfaccia backend - modifica dei destinatari per un cliente

.. note::
    È importante tenere a mente che le mail delle newsletter destinate a un cliente arriveranno **esclusivamente
    agli indirizzi email dei destinatari specificati in questa sezione**, e non agli indirizzi email ufficiali
    o del contatto, di cui alla sezione anagrafica descritta in precedenza.


Visualizzazione notizie che il cliente riceverà con il prossimo invio
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A beneficio dell'amministratore, in questa sezione sono elencate le notizie che il cliente riceverà al prossimo invio.

.. image:: images/amministratori_cliente_notizie.png
  :width: 760
  :alt: Interfaccia backend - visualizzazione delle notizie per un cliente

Modificando gli argomenti o il flag *vuole tutto* e premendo il tasto Salva, è possibile vedere come cambia l'elenco
delle notizie che verranno inviate.


Gestione utenti
---------------

Affinché un utente possa accedere al sistema, è necessario creare un'utenza, con un indirizzo email
e una password.

Questo vale sia per gli utenti amministratori e redattori, quando accedono all'interfaccia di backend,
sia per le utenze dei clienti, che devono accedere all'interfaccia di frontend.

Cliccando sul link **Utenti** si giunge all'elenco degli utenti già inseriti.

.. image:: images/amministratori_lista_utenti.png
  :width: 760
  :alt: Interfaccia backend - visualizzazione elenco utenti

Cliccando sul pulsante **Aggiungi utente** si arriva a un modulo dove è possibile inserire *username* e *password*
del nuovo utente.

La password deve essere inserita due volte, per conferma.
Non è possibile utilizzare password troppo simili alla username o password troppo semplici. Nel caso lo si faccia,
il sistema impedisce di proseguire e visualizza un messaggio di errore.

.. image:: images/amministratori_aggiungi_utente.png
  :width: 760
  :alt: Interfaccia backend - modulo aggiunta utente

Al salvataggio dei dati, si è condotti al modulo di *modifica di un utente*, dove è possibile completare
i dati dell'utente, aggiungendo il nome, l'indirizzo email e altri dati.

.. note::

   L'indirizzo email dell'utenza **non è l'indirizzo cui verranno inviate le newsletter**, che invece saranno
   inviate agli indirizzi dei destinatari, specificati nella sezione apposita dell'interfaccia di gestione del Cliente.


.. image:: images/amministratori_modifica_utente.png
  :width: 760
  :alt: Interfaccia backend - modifica dati utente

Nel caso di utenti *amministratori* o *redattori* c'è bisogno di

 - definire l'utente come utente di staff, tramite la checkbox *Priilegi di staff*,
 - assegnare l'utente al proprio gruppo, selezionandolo dalla casella *gruppi disponibili*
   e premendo la freccia verso destra, per trasportarlo verso la casella dei *gruppi selezionati*.

Questo può essere fatto nella sezione *Permessi* dell'interfaccia di modifica di un utente.

.. image:: images/amministratori_modifica_utente_permessi.png
  :width: 760
  :alt: Interfaccia backend - modifica dati utente - sezione Permessi

Nel caso di un'utenza creata per un cliente, è necessario associarla al cliente, usando il campo **Cliente**.
Occorre fare click sulla lente di ingrandimento accanto al campo, poi cliccare sul cliente selezionato,
all'interno della finestra che compare in popup. Il cliente può essere ricercato attraverso il motore di ricerca,
all'interno di quella stessa finestra.

.. image:: images/amministratori_utente_associa_cliente.png
  :width: 760
  :alt: Interfaccia backend - modifica dati utente - associa cliente

Cambio password
+++++++++++++++

Qualora fosse necessario cambiare manualmente la password di un utenza, è possibile farlo, facendo click sul link
"questo form", mostrato sotto il campo *password*.

.. image:: images/amministratori_utente_link_cambio_password.png
  :width: 760
  :alt: Interfaccia backend - modifica dati utente - link cambio password

Il link conduce a un modulo dove è possibile inserire la nuova password. Dal momento in cui si clicca su
**Cambia password**, l'utente dovrà utilizzare la nuova password per accedere al sistema.

.. image:: images/amministratori_utente_cambio_password.png
  :width: 760
  :alt: Interfaccia backend - modifica dati utente - modulo cambio password



Gestione Newsletter
-------------------

Tag
+++

I tag sono gli argomenti con cui sono categorizzate le notizie, e verso i quali i clienti esprimono il loro interesse.
Quando a una notizia viene aggiunto o rimosso un tag, o quando un cliente aggiunge o rimuove un tag al quale è
interessato, vengono *automaticamente* ricalcolati i destinatari, in mase all'*algoritmo di matching*.

.. note::

    L'**algoritmo di matching** seleziona, tra i destinatari di una news tutti i clienti che hanno espresso
    interesse per almeno un argomento (Tag) con cui la news è stata categorizzata.

L'elenco dei tag può essere gestito dagli utenti amministratori, facendo click sul link **Tag** della pagina principale
dell'interfaccia di backend.

Al click, viene mostrato l'elenco dei tag inseriti:

.. image:: images/amministratori_tags.png
  :width: 760
  :alt: Interfaccia backend - elenco tags

Un click su un tag permette la modifica del tag.

.. image:: images/amministratori_tag.png
  :width: 760
  :alt: Interfaccia backend - modifica tag

Per aggiungere un nuovo tag basta fare click sul pulsante **Aggiungi tag**.

Notizie
+++++++

Ciascun utente appartenente al gruppo *Amministratori* può creare o modificare una notizia, esattamente come
descritto nella :ref:`sezione Notizie per i redattori <notizie redattore>`.

News pronte per l'invio
^^^^^^^^^^^^^^^^^^^^^^^
L'amministratore è in grado, una volta verificata la correttezza della notizia, di impostaro lo status della
notizia a "Pronta per l'invio".

.. image:: images/amministratori_notizia_pronta.png
  :width: 760
  :alt: Interfaccia backend - modifica notizia status pronta

È anche possibile impostare diverse notizie come pronte per l'invio, contemporaneamente.
Per fare questo, occorre selezionare le notizie (la checkbox a sinistra del titolo, nell'elenco),
poi nella tendina *Azione* selezinare la voce "Imposta le notizie selezionate come pronte per l'invio".

.. image:: images/amministratori_notizie_seleziona_pronte.png
  :width: 760
  :alt: Interfaccia backend - seleziona notizie pronte

È naturalmente possibile, usando la voce "Imposta le notizie selezionate come non pronte per l'invio",
invertire la scelta.

.. image:: images/amministratori_notizie_seleziona_non_pronte.png
  :width: 760
  :alt: Interfaccia backend - seleziona notizie non pronte

La modifica del pool di notizie pronte o non pronte, cambia l'elenco delle news incluse nella newsletter da inviare.

La voce del menu *Azione* Calcola i destinatari delle notizie selezionate, effettua il calcolo automatico, in base
all'*algoritmo di matching* dei destinatari di tutte le notizie selezionate. Questo può essere utile nel caso in cui si
ritenga che i destinatari siano *disallineati* e ci sia bisogno di un ricalcolo manuale.


Categorie
+++++++++

Le *Categorie* sono usate per raggruppare le notizie all'interno di una Newsletter e costituiscono le *sezioni* o
i *paragrafi*, in modo da facilitare la lettura della newsletter all'utente.

L'amministratore può gestire le categorie, a partire dal link **Categorie** della pagina principale dell'interfaccia
di amministrazione.

.. image:: images/amministratori_categorie.png
  :width: 760
  :alt: Interfaccia backend - elenco categorie

Le categorie possono essere ordinate trascinandole verso l'alto o verso il basso con il mouse, utilizzando lo
spazio sotto la colonna *Priorità*, nell'elenco delle categorie.

Una nuova categoria può essere aggiunta cliccando sul pulsante **Aggiungi categoria**.

.. image:: images/amministratori_categoria_nuova.png
  :width: 760
  :alt: Interfaccia backend - nuova categoria

Categorie di una singola Newsletter
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'elenco di categorie di cui sopra, costituisce **la base** per tutte le newsletter.
Quando viene creata una nuova newsletter l'intero elenco è copiato in un elenco di categorie dedicato
alla singola newsletter.

L'utente amministratore può ordinare, sempre tramite lo stesso meccanismo di trascinamento dalla
colonna *priorità*, le categorie per una data newsletter.

Questo avrà effetto immediato sul modo in cui le notizie saranno ordinate e raggruppate nella newsletter.

.. image:: images/amministratori_categorie_newsletter.png
  :width: 760
  :alt: Interfaccia backend - ordinamento categorie singola newsletter


Singola Newsletter
++++++++++++++++++

Facendo click sul link "Newsletter" nella Home page per gli amministratori,
si apre la pagina con l'elenco delle newsletter.

.. image:: images/redattori_newsletters.png
  :width: 760
  :alt: Interfaccia backend - Elenco newsletter

L'elenco mostra tutte le newsletter, per ordine di data di invio decrescente, con la newsletter ancora non inviata
in testa (se esiste).

Cliccando sul link di una newsletter, si visualizza il dettaglio di una newsletter.
Sono possibili due casi:

1. La newsletter **è già stata inviata**. In questo caso la visualizzazione del dettaglio della newsletter compare
in modalità *sola lettura*.

    .. image:: images/redattori_newsletter_readonly.png
      :width: 760
      :alt: Interfaccia backend - Dettaglio newsletter sola lettura

2. La newlsetter **non è ancora stata inviata**. In questo caso alcuni dettagli della newsletter sono modificabili.

    .. image:: images/amministratori_newsletter.png
      :width: 760
      :alt: Interfaccia backend - Dettaglio newsletter modifica

Il titolo e l'eventuale testo descrittivo della
newsletter sono modificabili, ed è possibile:

- cliccare sul pulsante **Invia anteprima** - l'anteprima arriverà all'indirizzo email associato all'utenza dell'amministratore
- cliccare sul pulsane **Invia newsletter** - le email sono inviate a tutti i clienti.






