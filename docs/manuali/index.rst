Manuali
=======

L'accesso a tutte le interfacce è **protetto da password**. Occorre
effettuare il login, inserendo la username e la password che si è ricevuta, per accedere con il proprio ruolo,
di amministratore o di redattore o come utente:

Per l'interfaccia di backend, redattori e amministratori accedono
a partire dal link: https://monitoraggio.infoparlamento.com/admin

.. image:: images/backend_login.png
  :width: 760
  :alt: Login interfaccia backend


I clienti accedono all'interfaccia di frontend, per la visualizzazione degli archivi e
delle notizie, al link: https://monitoraggio.infoparlamento.com

.. image:: images/frontend_login.png
  :width: 760
  :alt: Login interfaccia frontend


.. toctree::
    :maxdepth: 2

    redattore.rst
    amministratore.rst
    cliente.rst
