Manuale utente per redattori
============================

Una volta effettuato il login, i redattori visualizzano queste sezioni dell'interfaccia di
amministrazione:

.. image:: images/redattori_home.png
  :width: 760
  :alt: Home page admin redattori

I redattori possono gestire le news e modificare il titolo o il testo descrittivo di una newsletter.

.. _notizie redattore:

Notizie
-------

Facendo click sul link **Notizie**, si apre l'elenco delle notizie già presenti.

.. image:: images/redattori_notizie.png
  :width: 760
  :alt: Interfaccia backend - Lista notizie

Sulla destra è presente una sezione con i _filtri_, attraverso i quali è possibile selezionare le notizie in base a:
- per *Data* cui la notizia fa riferimento
- per lo stato della notizia (Pronta per l'invio / Non pronta)
- per la *Data di invio* della notizia
- per la *Categoria*

Le notizie mostrate inizialmente sono solo quelle **non ancora inviate**.
Facendo click sulla colonna della tabella contenente le notizie, è possibile modificare il *criterio di ordinamento*
delle notizie visualizzate nella tabella.

Nuova notizia
+++++++++++++

Cliccando sul pulsante "Aggiungi notizia", viene visualizzato il modulo di inserimento di una nuova notizia:

.. image:: images/redattori_notizia_nuova.png
  :width: 760
  :alt: Interfaccia backend - Dettaglio nuova notizia


Un redattore può modificare tutte le informazioni di una notizia, tranne il suo status "È pronta", che può
essere modificato solamente dagli utenti amministratori.

Modifica notizia esistente
++++++++++++++++++++++++++

Se invece, nella pagina dell'elenco delle notizie, si clicca su una notizia,
si visualizza la pagina della notizia. Sono possibili due casi:

1. **La notizia è già stata inviata**. In questo caso il dettaglio della notizia è visualizzato in modalità *sola lettura*,
   perché non è possibile modificare una notizia già inviata ai clienti.

    .. image:: images/redattori_notizia_readonly.png
      :width: 760
      :alt: Interfaccia backend - Dettaglio notizia in sola lettura

2. **La notizia non è ancora stata inviata**. In questo caso è possibile effettuare le modifiche.

    .. image:: images/redattori_notizia_edit.png
      :width: 760
      :alt: Interfaccia backend - Dettaglio notizia in modifica

Associazione con Categoria
++++++++++++++++++++++++++

La categoria di una notizia viene associata selezionandola dalla tendina in cima al modulo di modifica della notizia.

.. image:: images/redattori_notizia_categoria.png
  :width: 760
  :alt: Interfaccia backend - Associazione categoria di una notizia


Gestione tag di una notizia
+++++++++++++++++++++++++++
Nella sezione **Relazioni con i tag** è possibile aggiungere o rimuovere i **Tag** (gli argomenti) con cui la notizia
è categorizzata.

.. image:: images/redattori_notizia_tag.png
  :width: 760
  :alt: Interfaccia backend - Dettaglio notizia - Sezione tag

Ogni volta che a una notizia viene aggiunto o rimosso un tag, viene automaticamente effettuato
un ricalcolo dei clienti destinatari, in base all'algoritmo di matching.

L'algoritmo prevede che i destinatari siano selezionati tra i clienti che sono stati categorizzati con almeno uno dei tag
con i quali è stata categorizzata la notizia.

L'elenco dei clienti destinatari è mostrato nella sezione **Clienti destinatari** della stessa pagina di gestione della
notizia (vedi più avanti).

Attraverso il pulsante **Aggiungi un altro Relazione con Tag** è possibile aggiungere altri tag. Una notizia può essere
infatti categorizzata con uno o più Tag.

Gestione file allegati
++++++++++++++++++++++

Nella sezione **File allegati a una notizia** sono gestiti i file da allegare a una notizia.

.. image:: images/redattori_notizia_allegati.png
  :width: 760
  :alt: Interfaccia backend - Dettaglio notizia - Sezione allegati

Un file allegato può essere selezionato attraverso il pulsante "Sfoglia", e a un file può essere assegnato un nome.
Se il nome non è assegnato, viene in automatico utilizzato il nome del file (esclusa l'estenzione).
Il nome è quindi *opzionale* e ne consigliamo l'utilizzo qualora il nome del file sia oscuro o non di facile comprensione.

I files che possono essere allegati sono tutti quelli che possono essere scaricati: documenti PDF, documenti Word,
files compressi (ZIP), anche immagini, che tuttavia non saranno visualizzate, ma solo messe a disposizione per
lo scaricamento.

Attraverso il pulsante **Aggiungi un altro File allegato a una notizia** è possibile aggiungere altri files.

Gestione clienti destinatari
++++++++++++++++++++++++++++

I clienti cui la notizia verrà inviata, calcolati automaticamene attraverso l'*agoritmo di matching*, sono visualizzati
nella sezione **Clienti destinatari**.

.. image:: images/redattori_notizia_destinatari.png
  :width: 760
  :alt: Interfaccia backend - Dettaglio notizia - Sezione clienti destinatari

È possibile **escludere** un cliente dall'elenco di destinatari previsti per questa notizia, spuntando la casella
"È escluso". In questo modo, anche se l'elenco dei clienti dovesse essere ricalcolato per questa notizia, in base alla
aggiunta o alla rimozione di un tag, il cliente rimarrà sempre tra quelli esclusi.

Attraverso il pulsante **Aggiungi un altro Cliente destinatario** è possibile aggiungere manualmente altri clienti
come destinatari di questa notizia, forzando l'algoritmo automatico.

Newsletter
----------
Facendo click sul link "Newsletter" nella Home page per i redattori, si apre la pagina con l'elenco delle newsletter.

.. image:: images/redattori_newsletters.png
  :width: 760
  :alt: Interfaccia backend - Elenco newsletter

L'elenco mostra tutte le newsletter, per ordine di data di invio decrescente, con la newsletter ancora non inviata
in testa (se esiste).

È possibile, per i redattori, visualizzare il dettaglio di una newsletter, cliccando sul link della newsletter.
Sono possibili due casi:

1. La newsletter **è già stata inviata**. In questo caso la visualizzazione del dettaglio della newsletter compare
in modalità *sola lettura*.

    .. image:: images/redattori_newsletter_readonly.png
      :width: 760
      :alt: Interfaccia backend - Dettaglio newsletter sola lettura

2. La newlsetter **non è ancora stata inviata**. In questo caso, sebbene i dettagli della newsletter
non siano modificabili (li può modificare solo un utente con i pemessi da amministratore),
è possibile inviare un'anteprima della newsletter via email.

    .. image:: images/redattori_newsletter_modifica.png
      :width: 760
      :alt: Interfaccia backend - Dettaglio newsletter modifica

Cliccando sul pulsante **Invia anteprima** - l'anteprima arriverà all'indirizzo email associato all'utenza del redattore.


