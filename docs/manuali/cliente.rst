Manuale utente per cliente
==========================

Una volta effettuato il login, il cliente è condotto alla pagina contenente l'elenco delle Newsletter inviate.

.. image:: images/clienti_elenco_newsletter.png
  :width: 760
  :alt: Interfaccia frontend - elenco

L'utente visualizza l'elenco per l'anno corrente. Qualora volesse vedere le newsletter inviate negli anni precedenti,
può farlo selezionando la tendina *Filtra per anno* sulla destra.

Le newsletter, che corrispondono agli invii, essendo diverse per ogni giorno, potranno arrivare a essere centinaia
per anno, e saranno quindi *paginate*. Ne saranno visualizzate 50 per pagina, ordinate per date decrescenti (le
ultime inviate in testa). Sarà possibile navigare tra le diverse pagine, usando lo strumento di navigazione,
che comparirà in fondo alla pagina quando il numero di newsletter in archivio supererà le 50.

Cliccando su una newsletter si va alla visualizzazione della newsletter, organizzata per paragrafi, in base alle
categorie.

.. image:: images/clienti_dettaglio_newsletter.png
  :width: 760
  :alt: Interfaccia frontend - dettaglio newsletter

Con un click sulla notizia, si visualizza il dettaglio della notizia.

.. image:: images/clienti_dettaglio_news.png
  :width: 760
  :alt: Interfaccia frontend - dettaglio news

In questo dettaglio i link agli allegati permettono di scaricarli sul proprio computer.

Motore di ricerca
-----------------

Nella barra in alto è presente un campo *Cerca*, che permette all'utente di effettuare ricerche all'interno dell'archivio
delle newsletter.

Digitando una o più parole chiave, vengono estratti i risultati che contengono la parola nel titolo della news,
nella descrizione testuale, in uno dei tag.

.. image:: images/clienti_ricerca.png
  :width: 760
  :alt: Interfaccia frontend - ricerca

Per ciascun risultato, è visibile la newsletter di appartenenza, la data di invio, il titolo, un estratto della descrizione.
Le parole chiave inserite nella ricerca sono evidenziate in giallo.

L'ordinamento è per data cui la notizia fa riferimento con le ultime notizie in cima.
In alto a sinistra è possibile modificare l'ordinamento, impostandolo in modo che siano le notizie più rilevanti,
rispetto alla parola chiave, quelle che compaiono in cima all'elenco.

Il calcolo della rilevanza è fatto con un algoritmo complesso, tipico dei motori di ricerca, che tiene conto del numero di volte
che la parola compare nel testo, della lunghezza del testo, della frequenza con cui il termine compare in tutti i testi.

Cliccando sul link della newsletter in un risultato di ricerca, si va alla visualizzazione della newsletter.

Cliccando sul titolo della news si va alla visualizzazione del dettaglio di quella news.

Un utente è in grado di visualizzare, tra i risultati di ricerca, solamente le notizie di cui è stato destinatario.
