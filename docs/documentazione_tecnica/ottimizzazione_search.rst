Ottimizzazione delle Relazioni Tag nel Modello News
===================================================

Questa documentazione descrive il processo di ottimizzazione per il modello ``News`` per evitare il calcolo
ad ogni richiesta dei vettori di testo e del ranking, che, con un numero crescente di contenuti era rapidamente
diventato ingestibile.

L'ottimizzazione implica l'aggiornamento dei campi ``tags_names`` e ``search_vector`` ogni volta che viene aggiunto o rimosso un record ``TagRel``.

Modifiche al Modello
--------------------

Il modello ``News`` è stato modificato per includere un metodo `update_tags_names_and_search_names` che aggiorni
sia i campi ``tags_names`` che ``search_vector``.

I metodi ``save`` e ``delete`` del modello ``TagRel`` sono stati sovrascritti per chiamare questo metodo
di aggiornamento quando si aggiungono o rimuovono dei tag a una notizia.

.. code-block:: python

    from django.db import models
    from django.contrib.postgres.search import SearchVectorField, SearchVector
    from .other_models import TagRel  # Modifica l'import a seconda della struttura del tuo progetto

    class News(models.Model):
        # ... campi esistenti ...

        tags_names = models.TextField(blank=True, default="")
        search_vector = SearchVectorField(null=True)

        def update_tags_names_and_search_vector(self):
            self.tags_names = ", ".join(tag_rel.tag.denominazione for tag_rel in self.tags_rels.all())
            self.search_vector = (
                SearchVector('title', weight='A') +
                SearchVector('text', weight='B') +
                SearchVector('tags_names', weight='C')
            )
            self.save()

    class TagRel(GenericRelatable, models.Model):
        # ... campi esistenti ...

        def save(self, *args, **kwargs):
            super().save(*args, **kwargs)
            self.update_news_tags_and_search_vector()

        def delete(self, *args, **kwargs):
            # store a reference to the related news before deletion
            related_news = self.content_object if isinstance(self.content_object, News) else None
            super().delete(*args, **kwargs)
            if related_news:
                related_news.update_tags_names_and_search_vector()

        def update_news_tags_and_search_vector(self):
            if isinstance(self.content_object, News):
                news_item = self.content_object
                news_item.update_tags_names_and_search_vector()

Implementazione dei Segnali
---------------------------

Il segnale per il modello ``News`` assicura che il campo `aearch_vector` sia aggiornato
in base ai valori dei campi  `title`, `text` e `tags_names` pria del salvataggio.

.. code-block:: python

    from django.db.models.signals import pre_save
    from django.dispatch import receiver
    from .models import News, TagRel  # Modifica l'import a seconda della struttura del tuo progetto

    @receiver(pre_save, sender=News)
    def update_search_vector(sender, instance, **kwargs):
        instance.search_vector = (
            SearchVector('title', weight='A') +
            SearchVector('tags_names', weight='B') +
            SearchVector('text', weight='C')
        )

Migrazione del Database
-----------------------

Dopo aver apportato queste modifiche, è necessario creare e applicare una nuova migrazione del database:

.. code-block:: shell

    python manage.py makemigrations
    python manage.py migrate

Aggiornamento dei campi nella tabella news
-------------------------------------------

I campi `tags_names` e `search_vector` vanno aggiornati per ogni notizia.
Si può definire il seguente management task:

.. code-block:: python

    from django.core.management.base import BaseCommand
    from myapp.models import News

    class Command(BaseCommand):
        help = 'Updates tags_names and search_vector for all News objects'

        def handle(self, *args, **kwargs):
            for news in News.objects.all():
                news.update_tags_names_and_search_vector()
                news.save()  # This will trigger the pre_save signal


Si può lanciare anche il seguente codice all'interno di una shell, usando tqdm per seguire l'andamento dell'aggiornamento.

.. code-block:: python

from tqdm import tqdm
for news in tqdm(News.objects.all()):
    news.update_tags_names_and_search_vector()
    news.save()  # This will trigger the pre_save signal


Ottimizzazione del codice della vista
-------------------------------------
Il codice che genera la query di ricerca, usando le funzionalità dell'ORM di django sugli indici testuali
di Postgresql, può essere riscrita in questo modo, utilizzando il campo `search_vector`, che a questo punto
contiene i valori dell'indice di testo.

.. code-block:: python

    def get_queryset(self):

        order_by = self.request.GET.get('order_by', 'date')
        query_string = self.request.GET.get('q', '')
        search_query = SearchQuery(query_string)

        # annotate news with full-text search arguments
        results = self.model.objects.annotate(
            search_rank=SearchRank(F('search_vector'), search_query),
            headline_title=SearchHeadline(
                'title', search_query,
                start_sel='<span class="headline">', stop_sel='</span>',
                highlight_all=True
            ),
            headline_text=SearchHeadline(
                'text', search_query,
                start_sel='<span class="headline">',
                stop_sel='</span>'
            )
        ).filter(
            is_ready=True,
            search_vector=search_query
        )

        # filter news according to users' visibility over them
        if (
            not self.request.user.is_superuser and
            not self.request.user.groups.filter(name='Redattori') and
            not self.request.user.groups.filter(name='Amministratori')
        ):
            results = results.filter(
                sent_at__isnull=False,
                customernews__customer=self.request.user.customer,
                customernews__is_excluded=False
            )

        # based on order_by parameters, sort news
        if order_by == 'date':
            results = results.order_by("-date", "-search_rank")
        else:
            results = results.order_by("-search_rank", "-date")
        return results[:50]


Conclusione
-----------

Con queste modifiche, i campi ``tags_names`` e ``search_vector`` del modello ``News``
vengono aggiornati automaticamente ogni volta che viene aggiunto, aggiornato o rimosso un record ``TagRel`` correlato.
Questo garantisce l'accuratezza e l'efficienza della funzionalità di ricerca full-text.
