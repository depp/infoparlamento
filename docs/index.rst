.. InfoParlamento documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentazione NLGest per InfoParlamento
========================================


.. toctree::
   :maxdepth: 2
   :caption: Contenuti:

   introduzione
   tutorial/index.rst
   manuali/index.rst
..    documentazione_tecnica/index.rst
..    assistenza/index.rst
