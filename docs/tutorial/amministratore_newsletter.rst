Amministratore verifica e invia newsletter
==========================================

Qui due video-tutorial per amministratori.

Il primo illustra il processo di verifica una notizia, ponendo il flag "È pronta" al valore di "vero",
per poi verificare che la notizia risulti tra quelle che saranno inviate con il prossimo invio.

Ponendo il flag a "falso" la notizia viene rimossa dalle notizie incluse nel prossimo invio.


.. video:: ../_static/TutorialAmministratoreVerificaNotizia.mp4
    :width: 700

Il secondo video mostra come creare una newsletter, aggiungere delle notizie, selezionando lo status come
"Pronte per l'invio", verificare l'ordinamento delle notizie, e i destinatari, aggiungendo le liste di esclusione
e lavorando con il flag "Vuole tutto", che permette a un utente di ricevere tutte le news.

Vengono illustrati i processi per:

- inviare un anteprima della notizia - la si invia all'indirizzo email
  dell'utente amministratore loggato in quel momento e contiene tutte le notizie);
- inviare le newsletter vere e proprie ai destinatari dei clienti - le email contengono solo le notizie destinate
  ai clienti, in accordo con l'algoritmo di matching, il flag "Vuole tutto" e le liste di esclusione.

.. video:: ../_static/TutorialAmministratoreInviaNewsletter.mp4
    :width: 700


.. raw:: html

    <hr>
