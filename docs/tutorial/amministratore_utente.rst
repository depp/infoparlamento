Amministratore aggiunge utente
==============================

Qui un video di un amministratore che aggiunge un nuovo utente.
Sono illustrati sia i casi in cui l'utenza da aggiungere è per un redattore o un altro amministratore,
sia il caso in cui l'utenza da aggiungere è un'utenza legata a un cliente già esistente.

.. video:: ../_static/TutorialAmministratoreCreaUtenza.mp4
    :width: 700

.. raw:: html

    <hr>
