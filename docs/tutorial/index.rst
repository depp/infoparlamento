Tutorial
========

Qui di seguito è possibile accedere a tre video-tutorial, per le operazioni più comuni di redattori e amministratori.

.. toctree::
    :maxdepth: 2

    redattore_notizia.rst
    amministratore_utente.rst
    amministratore_newsletter.rst
