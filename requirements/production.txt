# PRECAUTION: avoid production dependencies that aren't in development

-r base.txt

gunicorn==23.0.0 # https://github.com/benoitc/gunicorn
psycopg2==2.9.10  # https://github.com/psycopg/psycopg2
sentry-sdk==2.19.2  # https://github.com/getsentry/sentry-python

# Django
# ------------------------------------------------------------------------------
django-storages[boto3]==1.14.4  # https://github.com/jschneier/django-storages
django-anymail[amazon-ses,sendgrid,brevo]==12.0  # https://github.com/anymail/django-anymail
