#!/bin/bash

# Get container ID (assuming container name contains 'django' or your specific pattern)
CONTAINER_ID=$(docker ps -qf "name=infoparlamento_web")

if [ -z "$CONTAINER_ID" ]; then
    echo "infoparlamento_web container not found"
    exit 1
fi

# Execute management command inside the container
docker exec $CONTAINER_ID python manage.py cleanup_db
