from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import DateTimeField
from django.utils.translation import gettext_lazy as _

__author__ = "guglielmo"


class GenericRelatable(models.Model):
    """
    An abstract class that provides the possibility of generic relations
    """

    content_type = models.ForeignKey(ContentType, blank=True, null=True, db_index=True, on_delete=models.CASCADE,)
    object_id = models.PositiveIntegerField(null=True, db_index=True)
    content_object = GenericForeignKey("content_type", "object_id")

    class Meta:
        abstract = True


class Timestampable(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields.
    """

    created_at = DateTimeField(_("creation time"), auto_now_add=True)
    updated_at = DateTimeField(_("last modification time"), auto_now=True)

    class Meta:
        abstract = True


class Prioritisable(models.Model):
    """
    An abstract base class that provides an optional priority field,
    to impose a custom sorting order.
    """

    priority = models.SmallIntegerField(
        _("Priority"), null=True, blank=True, default=0, help_text=_("Sort order in case ambiguities arise")
    )

    class Meta:
        abstract = True
