from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from infoparlamento.newsletter.models import Newsletter, News, Category, NewsletterCategory, TagRel, Customer, Tag
from infoparlamento.newsletter.utils import sync_newsletter_categories


@receiver(post_save, sender=News)
def news__post_save__handler(sender, instance, **kwargs):
    """Handle post_save signal for News instances
    - category of the news is added or removed from NewsletterCategory
    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """

    nl = Newsletter.objects.filter(sent_at__isnull=True).first()
    if nl is None:
        return

    sync_newsletter_categories(nl)
    instance.update_tags_names_and_search_vector()


@receiver(post_save, sender=Newsletter)
def newsletter_creation_handler(sender, instance, **kwargs):
    """Handle post_save signal for Newsletter instances
    - news with is_ready set to true, that are not sent are assigned to this newsletter
    - category instances are copied in NewsletterCategory
    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """

    if kwargs['created']:
        # assign the news the just created newsletter
        news = News.objects.filter(is_ready=True, sent_at__isnull=True)
        news.update(newsletter_id=instance.id)

        # copy the categories into the categories for this instance
        original_categories = Category.objects.filter(
            disabled=False, id__in=news.values('category_id')
        ).values().order_by('priority')
        for oc in original_categories:
            oc['category_id'] = oc.pop('id')
            oc.pop('name')
            oc.pop('disabled')
            oc['newsletter_id'] = instance.id
        NewsletterCategory.objects.bulk_create(
            [NewsletterCategory(**oc) for oc in original_categories]
        )


@receiver(post_save, sender=TagRel)
def tag_add_handler(sender, instance, **kwargs):
    """
    Detect if a tag has been added to the news or customer

    recompute the recipients for all news involved

    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """
    news_content_type = ContentType.objects.get_for_model(News)
    if (
        isinstance(instance, TagRel) and
        instance.content_type_id == news_content_type.id  and
        kwargs['created']
    ):
        try:
            news = instance.content_object
        except AttributeError:
            news = News.objects.get(pk=instance.object_id)
        news.compute_and_copy_recipients()

    customer_content_type = ContentType.objects.get_for_model(Customer)
    if (
        isinstance(instance, TagRel) and
        instance.content_type_id == customer_content_type.id  and
        kwargs['created']
    ):
        for news in News.objects.filter(is_ready=True, sent_at__isnull=True, tags_rels__tag=instance.tag):
            news.compute_and_copy_recipients()


@receiver(post_delete, sender=TagRel)
def tag_delete_handler(sender, instance, **kwargs):
    """
    Detect if a tag has been added to the news, and reset, then recompute the recipients

    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """
    if isinstance(instance, TagRel) and isinstance(instance.content_object, News):
        news = instance.content_object
        news.customernews_set.filter(is_excluded=False).delete()
        news.compute_and_copy_recipients()

    if isinstance(instance, TagRel) and isinstance(instance.content_object, Customer):
        customer = instance.content_object
        for news in customer.news.filter(is_ready=True, sent_at__isnull=True, tags_rels__tag=instance.tag):
            news.customernews_set.filter(customer=customer, is_excluded=False).delete()
            news.compute_and_copy_recipients()


@receiver(pre_save, sender=Tag)
def pre_save_disable_tag_handler(sender, instance, **kwargs):
    """
    Capture the initial state
    """
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        # Object is new, so field hasn't technically changed
        pass
    else:
        instance._initial_disabled_state = obj.disabled


@receiver(post_save, sender=Tag)
def post_save_disable_tag_handler(sender, instance, **kwargs):
    """
    Detect if a tag disabled state has changed, and reset, then recompute the recipients for all the
    unsent news tagged with the modified Tag

    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """
    if hasattr(instance, '_initial_disabled_state'):
        if instance._initial_disabled_state != instance.disabled:
            news_content_type = ContentType.objects.get_for_model(News)
            tagged_news_ids = instance.related_objects.filter(content_type=news_content_type).values('object_id')
            tagged_news = News.objects.filter(pk__in=tagged_news_ids, sent_at__isnull=True)
            for news in tagged_news:
                news.customernews_set.filter(is_excluded=False).delete()
                news.compute_and_copy_recipients()


