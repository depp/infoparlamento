from django.contrib.admin.sites import AdminSite
from django.contrib.contenttypes.models import ContentType
from django.forms import Form
from django.test import TestCase, RequestFactory

from infoparlamento.newsletter.admin import TagRelInline
from infoparlamento.newsletter.forms import TagRelForm
from infoparlamento.newsletter.models import Tag, TagRel, News, Category
from infoparlamento.users.models import User


class TestTagRelForm(TestCase):

    def setUp(self):
        self.main_tag = Tag.objects.create(denominazione='main_tag')
        self.sub_tag1 = Tag.objects.create(denominazione='sub_tag1', parent=self.main_tag)
        self.sub_tag2 = Tag.objects.create(denominazione='sub_tag2', parent=self.main_tag)
        self.wrong_subtag = Tag.objects.create(denominazione='wrong_subtag')

    def test_init_no_instance(self):
        form = TagRelForm()
        self.assertEqual(form.fields['main_tag'].queryset.count(), 2)
        self.assertIn(self.main_tag, form.fields['main_tag'].queryset)
        self.assertIn(self.wrong_subtag, form.fields['main_tag'].queryset)
        self.assertEqual(form.fields['sub_tags'].queryset.count(), 0)

    def test_init_with_instance(self):
        tag_rel_instance = TagRel.objects.create(tag=self.sub_tag1)
        form = TagRelForm(instance=tag_rel_instance)
        self.assertEqual(form.fields['main_tag'].initial, self.main_tag)
        self.assertIn(self.sub_tag1, form.fields['sub_tags'].initial)

    def test_init_with_main_tag_data(self):
        form = TagRelForm(data={'main_tag': self.main_tag.pk})
        self.assertIn(self.sub_tag1, form.fields['sub_tags'].queryset)
        self.assertIn(self.sub_tag2, form.fields['sub_tags'].queryset)

    def test_clean_valid_data(self):
        form = TagRelForm(data={'main_tag': self.main_tag.pk, 'sub_tags': [self.sub_tag1.pk]})
        self.assertTrue(form.is_valid())

    def test_clean_invalid_subtags(self):
        form = TagRelForm(data={'main_tag': self.main_tag.pk, 'sub_tags': [self.wrong_subtag.pk]})
        self.assertFalse(form.is_valid())
        self.assertIn('sub_tags', form.errors)


class TestTagRelInlineFormSet(TestCase):
    def setUp(self):
        # Create a user for the News model
        self.author = User.objects.create(username="gianni", password="password")

        # Create a main tag
        self.main_tag = Tag.objects.create(
            denominazione="Main Tag",
            parent=None,
            disabled=False
        )

        # Create a sub tag
        self.sub_tag = Tag.objects.create(
            denominazione="Sub Tag",
            parent=self.main_tag,
            disabled=False
        )

        news_cat = Category.objects.create(name="Sample Category", disabled=False)

        # Create a News instance
        self.news = News.objects.create(
            number=1,
            title="Sample News",
            date="2023-10-01",
            text="This is a sample news text.",
            author=self.author,
            is_ready=True,
            is_ok_for_gianni=True,
            sent_at=None,
            category=news_cat  # Assuming that main_tag and category have the same requirements
        )

        # Get the content type for the News model
        self.content_type = ContentType.objects.get_for_model(News)

        # Prepare initial data to mimic form data that would be submitted
        self.data = {
            'tagrel_set-TOTAL_FORMS': '1',
            'tagrel_set-INITIAL_FORMS': '0',
            'tagrel_set-MIN_NUM_FORMS': '0',
            'tagrel_set-MAX_NUM_FORMS': '1000',
            'tagrel_set-0-main_tag': self.main_tag.id,
            'tagrel_set-0-sub_tags': self.sub_tag.id,
            'tagrel_set-0-content_type': str(self.content_type.pk),
            'tagrel_set-0-object_id': str(self.news.pk),
        }
        # Create a mock request with a mock user
        self.factory = RequestFactory()
        self.request = self.factory.get('/')
        self.request.user = User.objects.create_superuser(
            username='testuser', password='password', email='test@example.com'
        )

        # Instantiate the formset with the prepared data
        inline_admin = TagRelInline(admin_site=AdminSite(), parent_model=News)
        formset_class = inline_admin.get_formset(self.request)

        # Instantiate the formset with the prepared data
        self.formset = formset_class(data=self.data, instance=self.news, queryset=TagRel.objects.none())
        self.formset.forms = [Form()]


    def test_init(self):
        assert self.formset.new_objects == []
        assert self.formset.changed_objects == []
        assert self.formset.deleted_objects == []

    def test_consolidate_forms_by_main_tag(self):
        form1 = self.formset[0]
        form2 = self.formset[1]
        form3 = self.formset[2]
        form1.cleaned_data = {'main_tag': 'tag1', 'sub_tags': ['sub1', 'sub2']}
        form2.cleaned_data = {'main_tag': 'tag2', 'sub_tags': ['sub3', 'sub4']}
        form3.cleaned_data = {'main_tag': 'tag1', 'sub_tags': ['sub5', 'sub6']}
        self.formset._consolidate_forms_by_main_tag()
        self.assertEqual(len(self.formset.forms), 2)
        self.assertIn('tag1', [form.cleaned_data['main_tag'] for form in self.formset.forms])
        self.assertIn('tag2', [form.cleaned_data['main_tag'] for form in self.formset.forms])
        for form in self.formset.forms:
            if form.cleaned_data['main_tag'] == 'tag1':
                self.assertEqual(form.cleaned_data['sub_tags'], ['sub1', 'sub2', 'sub5', 'sub6'])
            elif form.cleaned_data['main_tag'] == 'tag2':
                self.assertEqual(form.cleaned_data['sub_tags'], ['sub3', 'sub4'])

    def test_consolidate_forms_by_main_tag_no_main_tag(self):
        form = self.formset[0]
        form.cleaned_data = {'main_tag': None, 'sub_tags': ['sub1', 'sub2']}
        self.formset._consolidate_forms_by_main_tag()
        self.assertEqual(form.fields[self.formset.FIELD_SUB_TAGS].queryset, Tag.objects.none())

    def test_consolidate_forms_by_main_tag_with_same_main_tag_and_different_sub_tags(self):
        form1 = self.formset[0]
        form2 = self.formset[1]
        form1.cleaned_data = {'main_tag': 'tag1', 'sub_tags': ['sub1', 'sub2']}
        form2.cleaned_data = {'main_tag': 'tag1', 'sub_tags': ['sub3', 'sub4']}
        self.formset._consolidate_forms_by_main_tag()
        self.assertEqual(len(self.formset.forms), 1)
        merged_form = self.formset.forms[0]
        expected_subs = ['sub1', 'sub2', 'sub3', 'sub4']
        self.assertCountEqual(merged_form.cleaned_data['sub_tags'], expected_subs)

