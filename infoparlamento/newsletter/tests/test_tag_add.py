from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.test import TestCase
from django.utils import timezone

from infoparlamento.newsletter.models import Category, Newsletter, News, Customer, Tag, TagRel
from infoparlamento.users.models import User


class TagAddHandlerTest(TestCase):

    def setUp(self):
        """
        Create a News and Customer instance with no tags in the setup phase
        """
        self.user = User.objects.create_user(
            username='testuser',
            password='password123',
            email='testuser@example.com'
        )

        self.category = Category.objects.create(
            name='Sample Category',
            disabled=False
            # Include any other required fields for Category
        )

        self.newsletter = Newsletter.objects.create(
            title='Sample Newsletter',
            introductory_text='This is an introductory text',
            sent_at=None  # Assuming it's not yet sent
            # Include any other required fields for Newsletter
        )

        self.news = News.objects.create(
            number=1,
            title='Sample News',
            date=timezone.now(),
            text='This is a sample news content',
            author=self.user,
            is_ready=True,
            category=self.category,  # Reference the created category instance
            newsletter=self.newsletter  # Reference the created newsletter instance
        )
        self.customer = Customer.objects.create(
            name='Sample Customer',
            company_codes='1234567890',
            eb_code='EB123',
            is_active=True,
            # Include all required fields for Customer
        )
        self.tag = Tag.objects.create(
            denominazione='Sample Tag',
        )


    def test_add_tag_to_news_only(self):
        """
        Add a tag to the news only, verify that news recipients are empty
        """
        # Create a TagRel instance related to the created Tag and News
        with transaction.atomic():
            news_content_type = ContentType.objects.get_for_model(News)
            TagRel.objects.create(
                tag=self.tag,
                content_type=news_content_type,
                object_id=self.news.id
            )

        # Assuming news has a method or attribute to get recipients
        recipients = self.news.compute_recipients()  # Or another method to get recipients
        self.assertEqual(recipients.count(), 0, "Recipients of the news should be empty")

    def test_add_same_tag_to_news_and_customer(self):
        """
        Add the same tag to both news and customer, verify that the recipients of the news
        are taken from the customer's recipients
        """
        with transaction.atomic():
            news_content_type = ContentType.objects.get_for_model(News)
            TagRel.objects.create(
                tag=self.tag,
                content_type=news_content_type,
                object_id=self.news.id
            )

            customer_content_type = ContentType.objects.get_for_model(Customer)
            TagRel.objects.create(
                tag=self.tag,
                content_type=customer_content_type,
                object_id=self.customer.id
            )

        recipients = self.news.compute_recipients() # Or another method to get recipients
        self.assertEqual(recipients.count(), 1)

    def test_add_same_tag_to_news_and_inactive_customer(self):
        """
        Add the same tag to both news and an inactive customer, verify that the recipients of the news
        are zero
        """
        inactive_customer = Customer.objects.create(
            name='Sample Customer',
            company_codes='1234567890',
            eb_code='EB123',
            is_active=False
        )

        with transaction.atomic():
            news_content_type = ContentType.objects.get_for_model(News)
            TagRel.objects.create(
                tag=self.tag,
                content_type=news_content_type,
                object_id=self.news.id
            )

            customer_content_type = ContentType.objects.get_for_model(Customer)
            TagRel.objects.create(
                tag=self.tag,
                content_type=customer_content_type,
                object_id=inactive_customer.id
            )


        # Assuming news has a method or attribute to get recipients
        recipients = self.news.compute_recipients() # Or another method to get recipients
        self.assertEqual(recipients.count(), 0)

    def test_add_different_tags_to_news_and_customer(self):
        """
        Add different tags to news and customer, verify that news recipients are empty
        """
        different_tag = Tag.objects.create(
            denominazione='Different Tag',
            disabled=False
        )
        with transaction.atomic():
            news_content_type = ContentType.objects.get_for_model(News)
            TagRel.objects.create(
                tag=self.tag,
                content_type=news_content_type,
                object_id=self.news.id
            )

            customer_content_type = ContentType.objects.get_for_model(Customer)
            TagRel.objects.create(
                tag=different_tag,
                content_type=customer_content_type,
                object_id=self.customer.id
            )

        # Assuming news has a method or attribute to get recipients
        recipients = self.news.compute_recipients()  # Or another method to get recipients
        self.assertEqual(recipients.count(), 0, "Recipients of the news should be empty if tags are different")

    def test_disable_enable_tag(self):
        """
        Add the same tag to both news and customer,
        verify that the number of recipients drop from 1 to 0 after the tag is disabled
        and returns to 1 if the tag is enabled back again
        """
        with transaction.atomic():
            news_content_type = ContentType.objects.get_for_model(News)
            TagRel.objects.create(
                tag=self.tag,
                content_type=news_content_type,
                object_id=self.news.id
            )

            customer_content_type = ContentType.objects.get_for_model(Customer)
            TagRel.objects.create(
                tag=self.tag,
                content_type=customer_content_type,
                object_id=self.customer.id
            )

        recipients = self.news.compute_recipients() # Or another method to get recipients
        self.assertEqual(recipients.count(), 1)

        self.tag.disabled = True
        self.tag.save()
        recipients = self.news.compute_recipients() # Or another method to get recipients
        self.assertEqual(recipients.count(), 0)

        self.tag.disabled = False
        self.tag.save()
        recipients = self.news.compute_recipients() # Or another method to get recipients
        self.assertEqual(recipients.count(), 1)
