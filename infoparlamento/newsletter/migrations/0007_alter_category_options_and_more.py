# Generated by Django 4.2.2 on 2023-07-05 08:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("newsletter", "0006_alter_category_options_news_author"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="category",
            options={"ordering": ["name"], "verbose_name": "section", "verbose_name_plural": "sections"},
        ),
        migrations.AlterModelOptions(
            name="newslettercategory",
            options={"verbose_name": "Newsletter section", "verbose_name_plural": "Newsletter sections"},
        ),
        migrations.AlterModelOptions(
            name="tag",
            options={"verbose_name": "Category", "verbose_name_plural": "Categories"},
        ),
        migrations.AlterModelOptions(
            name="tagrel",
            options={"verbose_name": "Category relation", "verbose_name_plural": "Category relations"},
        ),
        migrations.AlterField(
            model_name="category",
            name="name",
            field=models.CharField(help_text="Name of the section", max_length=64, verbose_name="Name"),
        ),
        migrations.AlterField(
            model_name="news",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="news",
                to="newsletter.category",
                verbose_name="Section",
            ),
        ),
        migrations.AlterField(
            model_name="news",
            name="number",
            field=models.PositiveSmallIntegerField(
                blank=True, default=0, help_text="A sequence number", null=True, verbose_name="Number"
            ),
        ),
        migrations.AlterField(
            model_name="news",
            name="title",
            field=models.TextField(help_text="A title for this news", max_length=512, verbose_name="Title"),
        ),
        migrations.AlterField(
            model_name="newslettercategory",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="newsletter.category", verbose_name="Section"
            ),
        ),
        migrations.AlterField(
            model_name="tag",
            name="denominazione",
            field=models.CharField(help_text="The name of the category", max_length=32, verbose_name="Name"),
        ),
        migrations.AlterField(
            model_name="tagrel",
            name="tag",
            field=models.ForeignKey(
                help_text="Instance of category assigned to this object",
                on_delete=django.db.models.deletion.CASCADE,
                related_name="related_objects",
                to="newsletter.tag",
                verbose_name="Category",
            ),
        ),
    ]
