# coding: utf-8
import json
from difflib import SequenceMatcher
from django.core.management.base import CommandError
from infoparlamento.newsletter.models import Tag
from infoparlamento.utils.management import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """Fetch categories data from json file, and import them as tags
    """

    help = "Import categories from local json file and create/update tags"
    dry_run = False

    def add_arguments(self, parser):
        # Adding a --dry-run option as a boolean flag
        parser.add_argument(
            '--dry-run',
            action='store_true',  # This makes it a flag (no value needed, True if present)
            help='Simulate the action without making any changes',
        )


    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, **options)

        self.dry_run = options['dry_run']

        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.info("Starting the tag import process")

        try:
            with open("resources/categories_structure.json", "r") as categories_file:
                categories_data = json.load(categories_file)
        except FileNotFoundError as e:
            self.logger.error(f"File not found: {e}")
            raise CommandError(f"File not found: {e}")
        except json.JSONDecodeError as e:
            self.logger.error(f"Error decoding JSON: {e}")
            raise CommandError(f"Error decoding JSON: {e}")

        def get_or_create_tag(denominazione, existing_tags, parent=None):

            # Check for similar tags
            for tag_denominazione, tag in existing_tags.items():
                similarity = SequenceMatcher(None, tag_denominazione, denominazione.lower()).ratio()

                update = False
                if similarity > 0.9:
                    if similarity < 1.0:
                        confirm = input(
                            f"{parent if parent else ''}/{tag.denominazione} "
                            f"is similar to {denominazione} ({similarity}). Proceed? [y/N] "
                        )

                        if confirm and confirm.lower()[0] == "y":
                            update = True
                        else:
                            continue
                    else:
                        update = True

                    if update and not self.dry_run:
                        tag.parent = parent
                        tag.disabled = False
                        tag.save()

                    self.logger.info(f"{parent if parent else ''}/{tag.denominazione} updated.")
                    return tag, False
                else:
                    continue

            # Create a new tag if no similar one is found
            if not self.dry_run:
                tag = Tag.objects.create(
                    denominazione=denominazione, parent=parent, disabled=False)
            else:
                tag = Tag(denominazione=denominazione, parent=parent, disabled=False)

            self.logger.info(f"{parent if parent else ''}/{denominazione} created.")

            existing_tags[denominazione.lower()] = tag  # Update the existing tags structure
            return tag, True


        Tag.objects.all().update(disabled=True)
        existing_tags_dict = {tag.denominazione.lower(): tag for tag in Tag.objects.all()}

        # main loop: tags are updated or created
        for main_category, subcategories in categories_data.items():
            main_tag, created_main = get_or_create_tag(main_category, existing_tags=existing_tags_dict)
            for sub_category in subcategories:
                sub_tag, created_sub = get_or_create_tag(
                    sub_category,
                    existing_tags=existing_tags_dict,
                    parent=main_tag
                )

        # after the loop all tags updated or created ar marked with disabled set to False
        # remove all tags marked as disabled, as they're not in the categories any longer
        if not self.dry_run:
            Tag.objects.filter(disabled=True).delete()

        self.logger.info("End of refactoring process")
