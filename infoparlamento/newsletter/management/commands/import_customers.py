# coding: utf-8
from datetime import datetime
import json

from infoparlamento.newsletter.models import Customer, CustomerRecipient
from infoparlamento.users.models import User
from infoparlamento.utils.management import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """Fetch customers data from json files, and import them
    """

    help = "Import customers from local json files"

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, **options)

        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.info("Starting ETL process")

        with open("resources/clienti.json", "r") as clienti_file:
            clienti_json = json.load(clienti_file)

        clienti = {}
        for c in clienti_json:
            client_name = c.pop('client_name')
            clienti[client_name] = c

        with open("resources/clienti_utenze.json", "r") as utenze_file:
            utenze_json = json.load(utenze_file)

        for u in utenze_json:
            try:
                cliente = clienti[u['client_name']]
            except KeyError:
                try:
                    cliente = clienti[u['client_name'].upper()]
                except KeyError:
                    self.logger.warn(f"Could not find {u['client_name']} among known customers")
                    continue

            if u['username'] != '-' and u['password'] != '-':
                cliente['user'] = {
                    'username': u['username'].split('@')[0],
                    'email': u['username'],
                    'password': u['password']
                }

        with open("resources/clienti_destinatari.json", "r") as destinatari_file:
            destinatari_json = json.load(destinatari_file)

        for d in destinatari_json:
            try:
                cliente = clienti[d['client_name']]
            except KeyError:
                try:
                    cliente = clienti[d['client_name'].upper()]
                except KeyError:
                    self.logger.warn(f"Could not find {d['client_name']} among known customers")
                    continue

            cliente['recipients'] = [
                r for r in d['emails']
            ]

        for c_name, c in clienti.items():
            c_obj, c_created = Customer.objects.update_or_create(
                name=c_name,
                defaults={
                    'address': c['client_address'],
                    'telephone': ", ".join(c.get('telephone_numbers', [])),
                    'contact_details': c['contact_details'],
                    'company_codes': ", ".join(c.get('company_codes', [])),
                    'contract_start_date': datetime.strptime(c['contract_date'], '%Y-%m-%d')
                }
            )

            if c_created:
                self.logger.info(f"<{c_name}> created")
            else:
                self.logger.info(f"<{c_name}> updated")

            if 'user' in c:
                u = c['user']
                u_obj = None
                try:
                    u_obj = User.objects.get(username=u['username'])
                except User.DoesNotExist:
                    u_obj = User.objects.create_user(
                        username=u['username'], password=u['password']
                    )
                    self.logger.info(f"  - new user <{u_obj.username}> created")
                else:
                    self.logger.info(f"  - user <{u_obj.username}> updated")
                finally:
                    u_obj.email = u['email']
                    u_obj.customer = c_obj
                    u_obj.save()

            if 'recipients' in c:
                for r in c['recipients']:
                    _, r_created = c_obj.recipients.get_or_create(email=r)
                    if r_created:
                        self.logger.info(f"  - recipient <{r}> added")

        self.logger.info("End of ETL process")
