from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    help = 'Cleanup old sessions and logs'

    def handle(self, *args, **options):
        with connection.cursor() as cursor:
            # Cleanup sessions
            cursor.execute("DELETE FROM django_session WHERE expire_date < NOW()")
            cursor.execute("REINDEX TABLE django_session")
            cursor.execute("VACUUM FULL django_session")

            # Cleanup admin logs
            cursor.execute("DELETE FROM django_admin_log WHERE action_time < NOW() - INTERVAL '10 days'")
            cursor.execute("REINDEX TABLE django_admin_log")
            cursor.execute("VACUUM FULL django_admin_log")

            # Cleanup audit logs
            cursor.execute("DELETE FROM auditlog_logentry WHERE timestamp < NOW() - INTERVAL '10 days'")
            cursor.execute("REINDEX TABLE auditlog_logentry")
            cursor.execute("VACUUM FULL auditlog_logentry")
