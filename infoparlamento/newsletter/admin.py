import os
from copy import deepcopy
from datetime import datetime

from adminsortable2.admin import SortableAdminMixin
from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin import TabularInline, SimpleListFilter
from django.contrib.contenttypes.admin import GenericTabularInline

from django.db import models
from django.db.models import FilteredRelation, Q, F
from django.forms import TextInput, Textarea
from django.http import HttpRequest, HttpResponseRedirect
from django.templatetags.static import static
from django.urls import resolve, reverse
from django.utils.html import format_html

from django.utils.translation import gettext_lazy as _

from infoparlamento.newsletter import jobs
from infoparlamento.newsletter.models import Newsletter, News, Category, NewsAttachmentFile, TagRel, Tag, Customer, \
    CustomerRecipient, CustomerNews, NewsletterCategory
from infoparlamento.newsletter.utils import sync_newsletter_categories
from infoparlamento.newsletter.forms import TagRelForm, TagRelInlineFormSet


class NewsAttachmentFileInline(TabularInline):
    model = NewsAttachmentFile
    show_change_link = True
    fields = ('name', 'file', )
    raw_id_fields = ('news', )
    extra = 0


class CustomerRecipientInline(TabularInline):
    model = CustomerRecipient
    show_change_link = True
    fields = ('name', 'email', )
    extra = 0


class CustomerNewsInline(TabularInline):
    model = CustomerNews
    verbose_name = _("Recipient customer")
    verbose_name_plural = _("Recipient customers")

    show_change_link = True
    fields = ('customer', 'is_excluded', 'is_manual', )
    raw_id_fields = ('customer', )
    extra = 0
    can_delete = False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.order_by('customer__name')
        return queryset

    class Media:
        css = {
            'all': ('css/custom_admin.css',)
        }


class NewsCustomersInline(TabularInline):
    model = CustomerNews
    verbose_name_plural = "Notizie che riceveranno con il prossimo invio"

    show_change_link = False
    fields = readonly_fields = ('news', 'news_category', )
    extra = 0
    max_num = 0
    can_delete = False

    @staticmethod
    @admin.display(description='categoria')
    def news_category(obj):
        return obj.news.category

    def get_queryset(self, request: HttpRequest):
        nl = Newsletter.objects.filter(sent_at__isnull=True).first()
        if nl:
            qs = super().get_queryset(request)
            return qs.filter(
                is_excluded=False,
                news__is_ready=True,
                news__newsletter=nl
            ).annotate(
                n=FilteredRelation(
                    "news__category__newslettercategory",
                    condition=Q(news__category__newslettercategory__newsletter=nl)
                )
            ).distinct().order_by('n__priority')
        else:
            return CustomerNews.objects.none()


class NewsInline(TabularInline):
    model = News
    verbose_name = _("News")
    verbose_name_plural = _("News")

    show_change_link = True
    fields = readonly_fields = ('title', 'category', 'date', 'is_ready', )
    extra = 0
    max_num = 0
    can_delete = False

    def get_queryset(self, request: HttpRequest):
        resolved = resolve(request.path_info)
        if "object_id" in resolved.kwargs:
            nl = self.parent_model.objects.get(pk=resolved.kwargs["object_id"])
            qs = super().get_queryset(request)
            return qs.filter(
                newsletter=nl
            ).annotate(
                n=FilteredRelation(
                    "category__newslettercategory",
                    condition=Q(category__newslettercategory__newsletter=nl)
                )
            ).distinct().order_by('n__priority', 'priority')
        else:
            return News.objects.none()


class TagRelInline(GenericTabularInline):
    model = TagRel
    form = TagRelForm
    formset = TagRelInlineFormSet
    fields = ('main_tag', 'sub_tags', 'get_disabled',)
    readonly_fields = ('get_disabled',)
    extra = 0
    template = 'admin/custom_tagrel_inline.html'

    def get_disabled(self, obj):
        return _('Yes') if obj.tag.disabled else _('No')

    get_disabled.short_description = _("Disabled")

    def has_change_permission(self, request, obj=None):
        if obj and isinstance(obj, News) and obj.sent_at:
            return False
        return super().has_change_permission(request, obj)

    def has_add_permission(self, request, obj=None):
        if obj and isinstance(obj, News) and obj.sent_at:
            return False
        return super().has_add_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        if obj and isinstance(obj, News) and obj.sent_at:
            return False
        return super().has_delete_permission(request, obj)

    class Media:
        css = {
            'all': ('admin/css/widgets.css',),  # Required CSS for the FilteredSelectMultiple
        }
        js = (
            'admin/js/core.js', 'admin/js/SelectBox.js', 'admin/js/SelectFilter2.js',
            static('js/custom_hierarchy.js')
        )


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'sent_at',
    )
    search_fields = (
        'title', 'introductory_text',
    )
    list_filter = ('sent_at', )
    readonly_fields = ('sent_at', )
    inlines = (NewsInline, )

    change_form_template = "newsletter/newsletter_change_form.html"
    save_on_top = True

    @staticmethod
    def view_on_site(obj):
        return reverse(
            'newsletter:newsletter_detail', kwargs={'pk': obj.pk}
        )

    def has_add_permission(self, request):
        num_objects = self.model.objects.filter(sent_at__isnull=True).count()
        if num_objects == 1:
            return False
        else:
            return True

    def has_change_permission(self, request, obj=None) -> bool:
        return self.has_delete_permission(request, obj)

    def has_delete_permission(self, request, obj=None) -> bool:
        if request.user.is_superuser:
            return True

        if (
            obj and obj.sent_at or
            not request.user.is_superuser and not request.user.groups.filter(name='Amministratori')
        ):
            return False
        else:
            return True

    def response_change(self, request, obj):
        domain = os.getenv('DOMAIN')
        if "_send-newsletter" in request.POST and obj.sent_at is None:
            try:
                if not settings.DEVELOPMENT:
                    jobs.send_newsletter_emails.delay(obj, preview=False, user=request.user)
                else:
                    jobs.send_newsletter_emails(obj, preview=False, user=request.user)
                self.message_user(request, "Newsletter inviata")
            except Exception as err:
                self.message_user(
                    request, f"Errore durante l'invio della newsletter: {err}",
                    level=messages.ERROR
                )
        if "_send-preview" in request.POST and obj.sent_at is None:
            try:
                jobs.send_newsletter_emails(obj, preview=True, user=request.user)
                self.message_user(request, "Anteprima della newsletter inviata")
            except Exception as err:
                self.message_user(
                    request, f"Errore durante l'invio: {err}",
                    level=messages.ERROR
                )

        return super().response_change(request, obj)


@admin.register(News)
class NewsAdmin(SortableAdminMixin, admin.ModelAdmin):

    list_display = (
        'priority', 'is_ok_for_gianni', 'title', 'category', 'date', 'is_ready', 'author', 'sent_at'
    )
    list_display_links = ('title', )
    search_fields = (
        'title', 'text'
    )
    list_per_page = 500
    list_filter = ('date', 'is_ready', 'is_ok_for_gianni', 'sent_at', 'category', )
    fields = ('category', 'title', 'text', 'date', 'is_ready', 'is_ok_for_gianni', 'sent_at', 'newsletter', )
    readonly_fields = ['sent_at', 'newsletter', ]
    ordering = ('priority', 'category' )

    inlines = (TagRelInline, NewsAttachmentFileInline, CustomerNewsInline)
    actions = ["duplicate_news", "set_recipients", "set_as_ready", "set_as_not_ready"]

    change_form_template = "newsletter/news_change_form.html"
    save_on_top = True

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '97'})},
        models.TextField: {'widget': Textarea(attrs={'cols': 80, 'rows': 5})}
    }

    class Media:
        css = {
            'all': (
                'css/admin_news.css',
            )
        }

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.select_related('author', 'newsletter', 'category')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """The category select dropdown shows them in alphabetical order

        :param db_field: The database field for which the form field is being constructed.
        :param request: The HttpRequest object representing the request.
        :param kwargs: Additional keyword arguments for customizing the form field creation.
        :return: A form field for the specified foreign key field, customized with the provided parameters when applicable.
        """
        if db_field.name == "category":
            kwargs["queryset"] = Category.objects.filter(disabled=False).order_by('priority')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    @staticmethod
    def view_on_site(obj):
        return reverse(
            'newsletter:news_detail', kwargs={'pk': obj.pk}
        )

    def changelist_view(self, request, extra_context=None):
        """Blank URL defaults to non-sent news"""
        test = request.META.get('HTTP_REFERER', '').split(request.META['PATH_INFO'])
        if len(request.GET) == 0 and (not test[-1].startswith('?') or test[-1] == '?sent_at__isnull=True'):
            return HttpResponseRedirect(f"{request.get_full_path()}?sent_at__isnull=True")

        response = super(NewsAdmin, self).changelist_view(request, extra_context=extra_context)
        return response

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        obj = self.get_object(request, object_id)
        if obj:
            extra_context['is_sent'] = obj.sent_at is not None
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def get_readonly_fields(self, request, obj=None):
        if request.user.groups.filter(name="Amministratori") or request.user.is_superuser:
            # self.message_user(request, "is_ready è modificabile")
            return ['sent_at', 'newsletter', ]
        else:
            # self.message_user(request, "is_ready non è modificabile")
            return ['sent_at', 'newsletter', 'is_ready']

    def has_change_permission(self, request, obj=None) -> bool:
        if obj and obj.sent_at:
            return False
        else:
            if not request.user.is_superuser and not request.user.groups.filter(name='Amministratori'):
                if obj and obj.is_ready:
                    return False
                else:
                    return True
            else:
                return True

    def has_delete_permission(self, request, obj=None) -> bool:
        if obj and obj.sent_at and not request.user.is_superuser:
            return False
        else:
            if not request.user.is_superuser and not request.user.groups.filter(name='Amministratori'):
                if obj and (obj.author != request.user or obj.is_ready):
                    return False
                else:
                    return True
            else:
                return True

    def set_recipients(self, request, queryset):
        """Compute selected news' recipients and copy them into CustomerNews"""
        news = queryset.filter(sent_at__isnull=True, is_ready=True)
        for n in news:
            n.compute_and_copy_recipients()
    set_recipients.short_description = _("Re-compute recipients for selected news")

    def set_as_ready(self, request, queryset):
        """Set selected news as ready to be sent"""
        try:
            nl = Newsletter.objects.get(sent_at__isnull=True)
        except Newsletter.DoesNotExist:
            nl = None

        news = queryset.filter(sent_at__isnull=True)
        news.update(is_ready=True, newsletter=nl)
        for n in news:
            n.compute_and_copy_recipients()

        if nl:
            sync_newsletter_categories(nl)
    set_as_ready.short_description = _("Set selected news as ready to be sent")

    def set_as_not_ready(self, request, queryset):
        """Set selected news as not ready to be sent"""
        queryset.filter(sent_at__isnull=True).update(is_ready=False, newsletter=None)

        try:
            nl = Newsletter.objects.get(sent_at__isnull=True)
        except Newsletter.DoesNotExist:
            nl = None
        if nl:
            sync_newsletter_categories(nl)
    set_as_not_ready.short_description = _("Set selected news as not ready to be sent")

    def duplicate_news(self, request, queryset):
        for news in queryset:
            new_obj = deepcopy(news)
            new_obj.id = None
            new_obj.is_ready = False
            new_obj.created_at = datetime.now()
            new_obj.updated_at = None
            new_obj.sent_at = None
            new_obj.save()

            for file in news.files.all():
                file_copy = deepcopy(file)
                file_copy.id = None
                file_copy.save()
                new_obj.files.add(file_copy)

            for tr in news.tags_rels.all():
                tr_copy = deepcopy(tr)
                tr_copy.id = None
                tr_copy.save()
                new_obj.tags_rels.add(tr)

            new_obj.save()
    duplicate_news.short_description = _("Duplicate selected news")

    def get_actions(self, request):
        # Disable delete
        actions = super(NewsAdmin, self).get_actions(request)

        if not request.user.is_superuser and not request.user.groups.filter(name='Amministratori'):
            del actions['set_as_ready']
            del actions['set_as_not_ready']
        return actions

    def response_change(self, request, obj):
        if "_send-all" in request.POST and obj.sent_at is None:
            try:
                if not settings.DEVELOPMENT:
                    jobs.send_single_news_to_all.delay(obj)
                else:
                    jobs.send_single_news_to_all(obj)
                self.message_user(request, "Notizia inviata a tutti")
            except Exception as err:
                self.message_user(
                    request, f"Errore durante l'invio della notizia: {err}",
                    level=messages.ERROR
                )
        return super().response_change(request, obj)

    def save_model(self, request, obj, form, change):
        """"""
        if obj.author is None:
            obj.author = request.user

        super().save_model(request, obj, form, change)

    def save_related(self, request, form, formsets, change) -> None:
        super().save_related(request, form, formsets, change)
        form.instance.compute_and_copy_recipients()


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'contract_start_date', 'contract_end_date', 'is_active', 'wants_it_all', 'impersonate_user'
    )
    search_fields = (
        'name', 'contact_details', 'email'
    )
    list_filter = ('is_active', 'wants_it_all')

    inlines = (TagRelInline, CustomerRecipientInline, NewsCustomersInline)
    list_per_page = 200
    ordering = ('name', )
    actions = ["set_as_inactive", "set_as_active", ]

    def __init__(self, model, admin_site):
        super().__init__(model, admin_site)
        self.request = None
        self._can_impersonate = None

    def get_queryset(self, request):
        self._can_impersonate = (
            request.user.groups.filter(name__in=['Redattori', 'Amministratori']).exists() or request.user.is_superuser
        )
        return (super(CustomerAdmin, self).
            get_queryset(request)
                .select_related('user')
        )

    def changelist_view(self, request, extra_context=None):
        """Blank URL defaults to non-active users"""
        test = request.META.get('HTTP_REFERER', '').split(request.META['PATH_INFO'])
        if len(request.GET) == 0 and (not test[-1].startswith('?') or test[-1] == '?is_active__exact=1'):
            return HttpResponseRedirect(f"{request.get_full_path()}?is_active__exact=1")

        return super(CustomerAdmin, self).changelist_view(request, extra_context=extra_context)

    def set_as_inactive(self, request, queryset):
        """Set selected customers as disabled"""
        queryset.update(is_active=False)
    set_as_inactive.short_description = _("Set selected customers as inactive")

    def set_as_active(self, request, queryset):
        """Set selected customers as enabled"""
        queryset.update(is_active=True)
    set_as_active.short_description = _("Set selected customers as active")

    def impersonate_user(self, obj):
        if not self._can_impersonate:
            return ""

        user_obj = obj.user
        if user_obj:
            url = reverse('users:impersonate-user', args=[user_obj.pk])
            return format_html(
                '<a href="{}" title="Visualizza il sito come lo vedrebbe {}">Anteprima  {}</a>',
                url, user_obj.username, user_obj.username
            )
        else:
            return ""
    impersonate_user.short_description = "Impersona"


@admin.register(Category)
class CategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('priority', 'name', 'disabled')
    list_display_links = ('name', )
    search_fields = ('name', )
    ordering = ('priority', )
    list_per_page = 500
    list_filter = ('disabled', )
    actions = ["set_as_disabled", "set_as_enabled", ]

    def changelist_view(self, request, extra_context=None):
        """Blank URL defaults to non-disabled categories"""
        test = request.META.get('HTTP_REFERER', '').split(request.META['PATH_INFO'])
        if len(request.GET) == 0 and (not test[-1].startswith('?') or test[-1] == '?disabled__exact=0'):
            return HttpResponseRedirect(f"{request.get_full_path()}?disabled__exact=0")

        return super(CategoryAdmin, self).changelist_view(request, extra_context=extra_context)

    def set_as_disabled(self, request, queryset):
        """Set selected sections as disabled"""
        queryset.update(disabled=True)
    set_as_disabled.short_description = _("Disable selected sections")

    def set_as_enabled(self, request, queryset):
        """Set selected sections as enabled"""
        queryset.update(disabled=False)
    set_as_enabled.short_description = _("Enable selected sections")


@admin.register(NewsletterCategory)
class NewsletterCategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('priority', 'category', )
    readonly_fields = ('category', 'newsletter', )
    search_fields = ('category__name', )
    ordering = ('priority', )

    def get_queryset(self, request: HttpRequest):
        qs = super().get_queryset(request)
        return qs.filter(newsletter__sent_at__isnull=True).order_by('priority')

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None) -> bool:
        if not request.user.is_superuser and not request.user.groups.filter(name='Amministratori'):
            return False
        else:
            return True

    def has_delete_permission(self, request, obj=None) -> bool:
        if request.user.is_superuser:
            return True
        else:
            return False


class SubTagInline(TabularInline):
    model = Tag
    fields = ('denominazione', 'disabled', )
    ordering = ('denominazione',)
    fk_name= 'parent'
    extra = 0


class TipoCategoriaFilter(SimpleListFilter):
    title = 'Tipo categoria'
    parameter_name = 'tipo_categoria'

    def lookups(self, request, model_admin):
        return (
            ('main', _('Categorie principali')),
            ('sub', _('Sotto-categorie')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'main':
            return queryset.filter(parent__isnull=True)
        if self.value() == 'sub':
            return queryset.filter(parent__isnull=False)
        return queryset

class ParentTagFilter(admin.SimpleListFilter):
    title = 'Categorie principali'
    parameter_name = 'parent'

    def lookups(self, request, model_admin):
        parent_tags = Tag.objects.filter(parent__isnull=True).order_by('denominazione')
        return [(tag.id, tag.denominazione) for tag in parent_tags]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(parent_id=self.value())
        return queryset


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('parent', 'denominazione', 'disabled', )
    list_display_links = ['denominazione', ]
    search_fields = ('denominazione', )
    ordering = ('parent__denominazione', 'denominazione', )
    list_filter = (TipoCategoriaFilter, ParentTagFilter, 'disabled', )
    inlines = [SubTagInline]
    actions = ["set_as_disabled", "set_as_enabled", ]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """The parent select dropdown only shows tags with no parent, in alphabetical order

        :param db_field: The database field for which the form field is being constructed.
        :param request: The HttpRequest object representing the request.
        :return: A form field for the specified foreign key field, customized with the provided parameters when applicable.
        """
        if db_field.name == "parent":
            kwargs["queryset"] = Tag.objects.filter(parent__isnull=True).order_by('denominazione')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def changelist_view(self, request, extra_context=None):
        """Handle main categories filter based on parent selection"""
        if request.GET.get('parent'):
            # Remove tipo_categoria if parent is selected
            params = request.GET.copy()
            if 'tipo_categoria' in params:
                del params['tipo_categoria']
                return HttpResponseRedirect(f"{request.path}?{params.urlencode()}")
        elif len(request.GET) == 0:
            # Default to main categories on fresh page load
            return HttpResponseRedirect(f"{request.get_full_path()}?tipo_categoria=main")

        return super().changelist_view(request, extra_context=extra_context)

    def set_as_disabled(self, request, queryset):
        """Set selected sections as disabled"""
        for tag in queryset:
            # tag._initial_disabled_state = tag.disabled
            tag.disabled = True
            tag.save()
    set_as_disabled.short_description = _("Disable selected categories")

    def set_as_enabled(self, request, queryset):
        """Set selected sections as enabled"""
        for tag in queryset:
            # tag._initial_disabled_state = tag.disabled
            tag.disabled = False
            tag.save()
    set_as_enabled.short_description = _("Enable selected categories")
