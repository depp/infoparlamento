from datetime import datetime
from typing import Any

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchHeadline
from django.core.exceptions import PermissionDenied
from django.db.models import FilteredRelation, Q, Count, F
from django.http import JsonResponse, HttpResponse
from django.template.loader import get_template
from django.views.generic import DetailView, ListView

from django.conf import settings
from weasyprint import HTML
from weasyprint.text.fonts import FontConfiguration

from infoparlamento.newsletter.models import News, Newsletter, Tag, Category


class NewsDetailView(LoginRequiredMixin, DetailView):
    model = News

    def get(self, request, *args, **kwargs):
        if (
            not self.request.user.is_superuser and
            not self.request.user.groups.filter(name='Redattori') and
            not self.request.user.groups.filter(name='Amministratori') and
            not self.get_object().customernews_set.filter(
                customer=self.request.user.customer,
                is_excluded=False
            )
        ):
            raise PermissionDenied

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['url_prefix'] = settings.EMAIL_URL_PREFIX
        context['news_tags'] = ", ".join([t.denominazione for t in self.get_object().tags])
        return context


news_detail_view = NewsDetailView.as_view()


class NewsPdfView(LoginRequiredMixin, DetailView):
    model = News
    template_name = 'newsletter/news_detail_pdf.html'

    def get(self, request, *args, **kwargs):
        if (
            not self.request.user.is_superuser and
            not self.request.user.groups.filter(name='Redattori') and
            not self.request.user.groups.filter(name='Amministratori') and
            not self.get_object().customernews_set.filter(
                customer=self.request.user.customer,
                is_excluded=False
            )
        ):
            raise PermissionDenied

        # Get the template
        template = get_template(self.template_name)

        # Get the object
        self.object = self.get_object()

        # Get context
        context = self.get_context_data()

        # Render template
        html_string = template.render(context)

        # Generate PDF
        font_config = FontConfiguration()
        html = HTML(string=html_string, base_url=request.build_absolute_uri('/'))
        pdf = html.write_pdf(font_config=font_config)

        # Create response
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = f'filename=news_{self.object.id}.pdf'
        return response

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['url_prefix'] = settings.EMAIL_URL_PREFIX
        context['news_tags'] = self.object.tags_names
        return context


news_pdf_view = NewsPdfView.as_view()

class NewsletterListView(LoginRequiredMixin, ListView):
    model = Newsletter
    paginate_by = settings.NEWSLETTERS_LIST_PAGE_SIZE

    def get_queryset(self):
        filter_year = int(self.request.GET.get('anno', datetime.now().year))
        queryset = self.model.objects.filter(
            Q(sent_at__year=filter_year) | Q(sent_at__isnull=True)
        )
        if (
            not self.request.user.is_superuser and
            not self.request.user.groups.filter(name='Redattori') and
            not self.request.user.groups.filter(name='Amministratori')
        ):
            queryset = queryset.filter(
                sent_at__isnull=False,
                news__customernews__customer=self.request.user.customer
            )
        return queryset.annotate(n_news=Count('news')).order_by('-sent_at')

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['filter_years'] = list(
            self.model.objects.order_by('-sent_at__year').values_list('sent_at__year', flat=True).distinct()
        )
        context['filter_selected_year'] = int(self.request.GET.get('anno', datetime.now().year))
        return context


newsletter_list_view = NewsletterListView.as_view()


class NewsletterDetailView(LoginRequiredMixin, DetailView):
    model = Newsletter

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)

        newsletter = self.get_object()
        news = newsletter.news.all()
        if (
            not self.request.user.is_superuser and
            not self.request.user.groups.filter(name='Redattori') and
            not self.request.user.groups.filter(name='Amministratori')
        ):
            news = news.filter(
                customernews__customer=self.request.user.customer,
                customernews__is_excluded=False
            )

        context['newsletter'] = newsletter
        context['newsletter_news'] = news.annotate(
            n=FilteredRelation(
                "category__newslettercategory",
                condition=Q(category__newslettercategory__newsletter=newsletter)
            )
        ).order_by('n__priority', 'n', 'number')
        return context


newsletter_detail_view = NewsletterDetailView.as_view()


class NewsSearchView(ListView):
    model = News
    template_name = "newsletter/news_search.html"

    # def get_queryset(self):
    #
    #     order_by = self.request.GET.get('order_by', 'date')
    #     query_string = self.request.GET.get('q', '')
    #     tag_filter = self.request.GET.getlist('tag')
    #     search_query = SearchQuery(query_string, search_type='websearch')
    #
    #     # annotate news with full-text search arguments
    #     results = self.model.objects.annotate(
    #         search_rank=SearchRank(F('search_vector'), search_query),
    #         headline_title=SearchHeadline(
    #             'title', search_query,
    #             start_sel='<span class="headline">', stop_sel='</span>',
    #             highlight_all=True
    #         ),
    #         headline_text=SearchHeadline(
    #             'text', search_query,
    #             start_sel='<span class="headline">',
    #             stop_sel='</span>'
    #         )
    #     ).filter(
    #         is_ready=True,
    #         search_vector=search_query
    #     )
    #
    #     if tag_filter:
    #         results = results.filter(tags_rels__tag__denominazione__in=tag_filter)
    #
    #     # filter news according to users' visibility over them
    #     if (
    #         not self.request.user.is_superuser and
    #         not self.request.user.groups.filter(name='Redattori') and
    #         not self.request.user.groups.filter(name='Amministratori')
    #     ):
    #         results = results.filter(
    #             sent_at__isnull=False,
    #             customernews__customer=self.request.user.customer,
    #             customernews__is_excluded=False
    #         )
    #
    #     # based on order_by parameters, sort news
    #     if order_by == 'date':
    #         results = results.order_by("-date", "-search_rank")
    #     else:
    #         results = results.order_by("-search_rank", "-date")
    #     return results[:settings.NEWSLETTERS_SEARCH_RESULTS_PER_PAGE]

    def get_base_queryset(self):
        """Get the base queryset for computing facets, before pagination but after permissions"""
        query_string = self.request.GET.get('q', '')
        search_query = SearchQuery(query_string, search_type='websearch')

        # Start with all ready news that match the search
        base_qs = self.model.objects.annotate(
            search_rank=SearchRank(F('search_vector'), search_query),
            headline_title=SearchHeadline(
                'title', search_query,
                start_sel='<span class="headline">', stop_sel='</span>',
                highlight_all=True
            ),
            headline_text=SearchHeadline(
                'text', search_query,
                start_sel='<span class="headline">',
                stop_sel='</span>'
            )
        ).filter(
            is_ready=True,
            search_vector=search_query
        )

        # Apply permission filtering
        if not (self.request.user.is_superuser or
                self.request.user.groups.filter(name__in=['Redattori', 'Amministratori'])):
            base_qs = base_qs.filter(
                sent_at__isnull=False,
                customernews__customer=self.request.user.customer,
                customernews__is_excluded=False
            )

        return base_qs

    def get_queryset(self):
        base_qs = self.get_base_queryset()

        # Apply category filter if selected
        selected_categories = self.request.GET.getlist('category')
        if selected_categories:
            base_qs = base_qs.filter(category__name__in=selected_categories)

        self.total_filtered_count = base_qs.count()

        # Apply ordering
        order_by = self.request.GET.get('order_by', 'date')
        if order_by == 'date':
            base_qs = base_qs.order_by("-date", "-search_rank")
        else:
            base_qs = base_qs.order_by("-search_rank", "-date")

        return base_qs[:settings.NEWSLETTERS_SEARCH_RESULTS_PER_PAGE]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query_string = self.request.GET.get('q', '')
        selected_categories = self.request.GET.getlist('category')

        # Get base queryset for faceting
        base_qs = self.get_base_queryset()

        # Store the total count without category filters
        total_unfiltered_count = base_qs.count()

        # Get categories and their counts
        category_counts = Category.objects.filter(
            disabled=False,
            name__startswith='PARLAMENTO:'
        ).annotate(
            count=Count(
                'news',
                filter=Q(news__in=base_qs)
            )
        ).filter(
            count__gt=0
        ).values(
            'name',
            'count'
        ).order_by('-count', 'name')

        total_count = self.total_filtered_count if selected_categories else total_unfiltered_count
        context.update({
            'query': query_string,
            'query_lower': query_string.lower(),
            'order_by': self.request.GET.get('order_by', 'date'),
            'selected_categories': selected_categories,
            'category_facets': category_counts,
            'total_count': total_count,
        })
        return context

news_search_view = NewsSearchView.as_view()


def get_subtags(request):
    main_tag_id = request.GET.get('main_tag_id')
    subtags = Tag.objects.filter(parent_id=main_tag_id).values('id', 'denominazione').order_by('denominazione')
    return JsonResponse({'subtags': list(subtags)})
