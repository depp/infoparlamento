import logging

from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.contenttypes.forms import BaseGenericInlineFormSet
from django.contrib.contenttypes.models import ContentType
from django.db import transaction, IntegrityError
from django.forms import ModelForm
from .models import TagRel, Tag

logger = logging.getLogger(__name__)

class TagRelForm(ModelForm):
    """
    A ModelForm for managing TagRel objects that allows users to select a main category (main_tag)
    and associated subcategories (sub_tags).

    The subcategories field dynamically updates its queryset based on the selected main category.

    Attributes:
        main_tag: A ModelChoiceField that allows selection of top-level tags.
            This field is required and labeled as "Main Category".
            Provides help text to assist users in understanding the purpose of the field.
        sub_tags: A ModelMultipleChoiceField for selecting subcategories linked to the chosen main category.
            This field is not required and has a custom widget for filtering and selecting multiple options.
            It is labeled as "Subcategories" with corresponding help text.

    Meta:
        model: Specifies the model to be used by this form, which is TagRel.
        fields: A list of fields to be included in the form - ['main_tag', 'sub_tags'].

    Methods:
        __init__(*args, **kwargs): Initializes the form and sets the queryset for sub_tags
            based on the selected main_tag. If editing an existing instance,
            ensures the correct initial values for sub_tags and main_tag.

        clean(): Validates that the selected subcategories (sub_tags) belong to the chosen main category (main_tag).
            Adds errors to the form if invalid subcategories are selected.
    """
    main_tag = forms.ModelChoiceField(
        queryset=Tag.objects.filter(parent__isnull=True, disabled=False).order_by("denominazione"),  # Only top-level tags
        required=True,
        label="Categoria principale",
        help_text="Select the main category."
    )
    sub_tags = forms.ModelMultipleChoiceField(
        queryset=Tag.objects.none(),
        required=False,
        widget=FilteredSelectMultiple("Sotto-categoria", is_stacked=False),
        label="Sottocategorie",
        help_text="Select subcategories related to the main category."
    )

    class Meta:
        model = TagRel
        fields = ['main_tag', 'sub_tags']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance.pk:
            try:
                # For sent news, we want to show the existing tag regardless of parent/child relationship
                if hasattr(self.instance.content_object, 'sent_at') and self.instance.content_object.sent_at:
                    self.initial['main_tag'] = self.instance.tag.parent if self.instance.tag.parent else self.instance.tag
                    if self.instance.tag.parent:
                        self.initial['sub_tags'] = [self.instance.tag]
                    else:
                        self.initial['sub_tags'] = []
                    return

                # Regular initialization for non-sent news
                if self._handle_main_tag_from_data():
                    return
                elif self.instance.tag.parent:
                    main_tag = self.instance.tag.parent
                    self._initialize_form_fields(main_tag)
            except (AttributeError, Tag.DoesNotExist):
                pass
        elif self.data:
            self._handle_main_tag_from_data()

    def _handle_main_tag_from_data(self):
        """
        Handle main tag initialization from form data.

        :return: True if main tag was handled, False otherwise
        """
        main_tag_field_name = self.add_prefix('main_tag')
        if main_tag_field_name in self.data:
            main_tag_id = self._parse_main_tag_id(self.data.get(main_tag_field_name))
            if main_tag_id:
                self._set_sub_tags_queryset(main_tag_id)
                return True
        return False

    def _initialize_form_fields(self, main_tag):
        """
        :param main_tag: The main tag object used to initialize the form fields.
        :return: None
        """
        self.fields['sub_tags'].queryset = main_tag.subtags.all().order_by('denominazione')
        self.fields['main_tag'].initial = main_tag
        selected_sub_tags = Tag.objects.filter(
            id__in=TagRel.objects.filter(
                content_type=self.instance.content_type,
                object_id=self.instance.object_id,
                tag__parent=main_tag
            ).values_list('tag', flat=True)
        ).order_by('denominazione')
        self.fields['sub_tags'].initial = selected_sub_tags

    @staticmethod
    def _parse_main_tag_id(main_tag_id_str):
        """
        :param main_tag_id_str: A string representation of the main tag ID.
        :return: The main tag ID as an integer if parsing is successful, otherwise None.
        """
        try:
            return int(main_tag_id_str)
        except (ValueError, TypeError):
            return None

    def _set_sub_tags_queryset(self, main_tag_id):
        """
        :param main_tag_id: The ID of the main tag used to filter sub tags.
        :return: None
        """
        self.fields['sub_tags'].queryset = Tag.objects.filter(
            parent_id=main_tag_id, disabled=False
        ).order_by('denominazione')

    def clean(self):
        """
        Cleans the form data and performs custom validation to ensure that the selected
        subcategories belong to the selected main category.

        :return: Cleaned data after custom validation.
        """
        cleaned_data = super().clean()
        main_tag = cleaned_data.get('main_tag')
        sub_tags = cleaned_data.get('sub_tags')
        if main_tag and sub_tags:
            valid_sub_tags = Tag.objects.filter(parent=main_tag).order_by('denominazione')
            if not all(tag in valid_sub_tags for tag in sub_tags):
                self.add_error(
                    'sub_tags', "Selected subcategories do not belong to the selected main category."
                )
        return cleaned_data


class TagRelInlineFormSet(BaseGenericInlineFormSet):
    """
           Custom Inline Form Set for handling Tag Relationships.
           This class is responsible for managing inline forms that handle the relationships
           between main tags and their associated sub-tags. It ensures that forms with
           the same main tag are consolidated, and it provides methods for saving these
           relationships safely, using transactional integrity.
           Attributes:
           - new_objects (list): List to keep track of newly created TagRel instances.
           - changed_objects (list): List to keep track of changed TagRel instances.
           - deleted_objects (list): List to keep track of deleted TagRel instances.
           Methods:
           - __init__(*args, **kwargs): Initializes the inline form set and consolidates forms
                                        with the same main tag.
           - save(commit=True): Saves the TagRel instances, ensuring transactional integrity
                                and handling deletions appropriately.
    """
    new_objects = []
    changed_objects = []
    deleted_objects = []

    FIELD_SUB_TAGS = 'sub_tags'
    FIELD_MAIN_TAG = 'main_tag'
    FIELD_DELETE = 'DELETE'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Skip consolidation for sent news
        if self.instance and hasattr(self.instance, 'sent_at') and self.instance.sent_at:
            return

        self._consolidate_forms_by_main_tag()

    def _consolidate_forms_by_main_tag(self):
        """
        Consolidates the forms in `self.forms` by their main tag.

        The method iterates through `self.forms`, retrieves the main tag of each form, and consolidates
        them into unique_forms based on the main tag. If a main tag is present, it delegates handling
        to _handle_existing_main_tag. For forms without a main tag, it sets the queryset of
        self.FIELD_SUB_TAGS to none. Finally, it updates `self.forms` with the consolidated values.

        Note: This method is skipped for sent news to preserve their original tag relationships.

        :return: None
        """
        unique_forms = {}
        for form in self.forms:
            main_tag = self._get_main_tag(form)
            if main_tag:
                self._handle_existing_main_tag(unique_forms, form, main_tag)
            else:
                form.fields[self.FIELD_SUB_TAGS].queryset = Tag.objects.none()
        self.forms = list(unique_forms.values())

    def _get_main_tag(self, form):
        """
        :param form: A form instance that contains the data being processed.
            The form should have an instance attribute that includes the tag information.
        :return: Returns the main tag associated with the form data. If the form instance has a primary key (pk),
            the method returns the parent tag if it exists, otherwise, it returns the tag itself.
            If the form has been submitted and is valid but the instance does not have a pk,
            the main tag is extracted from the cleaned data. Returns None if the form is not bound or not valid.
        """
        if form.instance.pk:
            try:
                tag = form.instance.tag
                return tag.parent if tag.parent else tag
            except (AttributeError, TagRel.tag.RelatedObjectDoesNotExist):
                return None
        else:
            return self._get_main_tag_from_cleaned_data(form) if form.is_bound and form.is_valid() else None

    def _get_main_tag_from_cleaned_data(self, form):
        """
        :param form: The form object containing potential cleaned_data attribute.
        :return: The main tag from the form's cleaned_data if available, otherwise from self.cleaned_data.
        """
        return form.cleaned_data.get(self.FIELD_MAIN_TAG, None) if hasattr(form,
                                                                           'cleaned_data') else self.cleaned_data.pop(
            0).get(self.FIELD_MAIN_TAG, None)

    def _handle_existing_main_tag(self, unique_forms, form, main_tag):
        """
        :param unique_forms: A collection of unique forms to be processed.
        :param form: The current form being handled.
        :param main_tag: The main tag identifier for the form.
        :return: None
        """
        if main_tag in unique_forms:
            self._consolidate_sub_tags(unique_forms, form, main_tag)
            self._hide_form_fields(form)
        else:
            self._initialize_main_tag_form(unique_forms, form, main_tag)

    def _consolidate_sub_tags(self, unique_forms, form, main_tag):
        """
        :param unique_forms: Dictionary containing the consolidated forms with main tags as keys
        :param form: Form object that contains the sub tags to be consolidated
        :param main_tag: The main tag which serves as the key in the unique_forms dictionary
        :return: None
        """
        if form.fields[self.FIELD_SUB_TAGS].initial:
            unique_forms[main_tag].fields[self.FIELD_SUB_TAGS].initial |= set(
                form.fields[self.FIELD_SUB_TAGS].initial.values_list('pk', flat=True))

    def _hide_form_fields(self, form):
        """
        :param form: A form instance whose specified fields need to be hidden.
        :return: None

        """
        form.fields[self.FIELD_MAIN_TAG].widget = forms.HiddenInput()
        form.fields[self.FIELD_SUB_TAGS].widget = forms.HiddenInput()

    def _initialize_main_tag_form(self, unique_forms, form, main_tag):
        """
        :param unique_forms: A dictionary where keys are main tags and values are their associated forms.
        :param form: The current form being initialized.
        :param main_tag: The primary tag to be set for the form.
        :return: None
        """
        unique_forms[main_tag] = form
        form.fields[self.FIELD_MAIN_TAG].initial = main_tag
        form.fields[self.FIELD_SUB_TAGS].queryset = main_tag.subtags.all()
        if form.instance.pk:
            form.fields[self.FIELD_SUB_TAGS].initial = set(
                TagRel.objects.filter(
                    content_type=form.instance.content_type,
                    object_id=form.instance.object_id,
                    tag__parent=main_tag
                ).order_by('tag__denominazione').values_list('tag', flat=True)
            )
        else:
            form.fields[self.FIELD_SUB_TAGS].initial = (
                form.cleaned_data['sub_tags']
                if hasattr(form, 'cleaned_data') and 'sub_tags' in form.cleaned_data else []
            )

    def save(self, commit=True):
        """
        :param commit: A boolean flag indicating if the save operation should be committed to the database.
            Default is True.
        :return: A list of instances that were created or updated during the save operation.
        """
        instances = []
        try:
            with transaction.atomic():
                if not self.instance.pk:
                    self.instance.save()
                self._handle_deletions()
                instances.extend(self._handle_creations_and_updates(commit))
        except IntegrityError as e:
            logger.error(f"Transaction failed due to integrity error: {e}")
        return instances

    def _handle_deletions(self):
        """
        Handles the deletion of instances in forms.

        If a form has been marked for deletion and the instance has a primary key,
        this method will call the function to delete the tag relationship instance for that form.

        :return: None
        """
        for form in self.forms:
            if form.cleaned_data.get(self.FIELD_DELETE, False) and form.instance.pk:
                self._delete_tag_rel_instance(form)

    @staticmethod
    def _delete_tag_rel_instance(form):
        """
        :param form: The form instance containing a tag relational instance to be deleted.
        :return: None
        """
        try:
            if form.instance.tag.parent:
                TagRel.objects.filter(
                    content_type_id=form.instance.content_type_id,
                    object_id=form.instance.object_id,
                    tag__parent=form.instance.tag.parent
                ).delete()
            else:
                form.instance.delete()
            logger.info(f"Deleted TagRel instance with pk: {form.instance.pk}")
        except Exception as e:
            logger.error(f"Error during deletion: {e}")

    def _handle_creations_and_updates(self, commit):
        """
        :param commit: A boolean value indicating whether to commit the database save operation
        :return: A list of instances created or updated based on the form data
        """
        instances = []
        for form in self.forms:
            if form.cleaned_data.get(self.FIELD_DELETE, False):
                continue
            if not self.instance.pk:
                self.instance.save()
            content_type_id = ContentType.objects.get_for_model(self.instance).pk
            object_id = self.instance.pk
            main_tag, sub_tags = self._get_tags_from_form(form)
            instances.extend(self._update_tag_rels(main_tag, sub_tags, content_type_id, object_id, commit))
        return instances

    def _get_tags_from_form(self, form):
        if form.cleaned_data and form.cleaned_data.get(self.FIELD_MAIN_TAG) is not None:
            return form.cleaned_data.get(self.FIELD_MAIN_TAG), form.cleaned_data.get(self.FIELD_SUB_TAGS)
        else:
            return form.fields[self.FIELD_MAIN_TAG].initial, form.fields[self.FIELD_SUB_TAGS].initial

    def _update_tag_rels(self, main_tag, sub_tags, content_type_id, object_id, commit):
        instances = []
        if main_tag and sub_tags:
            instances.extend(
                self._handle_tag_rels_creation_or_update(main_tag, sub_tags, content_type_id, object_id, commit))
        elif main_tag:
            self._delete_sub_tag_rels(main_tag, content_type_id, object_id)
            instances.extend(self._create_single_tag_rel(main_tag, content_type_id, object_id))
        return instances

    @staticmethod
    def _delete_sub_tag_rels(main_tag, content_type_id, object_id):
        TagRel.objects.filter(
            content_type_id=content_type_id,
            object_id=object_id,
            tag__parent=main_tag
        ).delete()

    def _handle_tag_rels_creation_or_update(self, main_tag, sub_tags, content_type_id, object_id, commit):
        existing_tag_rels = TagRel.objects.filter(
            content_type_id=content_type_id,
            object_id=object_id,
            tag__parent=main_tag
        )
        existing_tags = set(existing_tag_rels.values_list('tag', flat=True))
        new_tags = set(sub_tags.values_list('pk', flat=True))
        tags_to_add = new_tags - existing_tags
        tags_to_remove = existing_tags - new_tags
        self._delete_tag_rel_instances(tags_to_remove, existing_tag_rels)

        if not new_tags:
            # new_tags is empty, thus create a TagRel for the main_tag
            return self._create_single_tag_rel(main_tag, content_type_id, object_id)

        return self._create_tag_rel_instances(tags_to_add, content_type_id, object_id, commit)

    @staticmethod
    def _delete_tag_rel_instances(tags_to_remove, existing_tag_rels):
        for tag_id in tags_to_remove:
            tag_to_delete = existing_tag_rels.filter(tag_id=tag_id)
            tag_to_delete.delete()
            logger.info(f"Deleted TagRel instance with tag_id: {tag_id}")

    @staticmethod
    def _create_tag_rel_instances(tags_to_add, content_type_id, object_id, commit):
        instances = []
        for tag_id in tags_to_add:
            tag_rel_instance = TagRel(
                content_type_id=content_type_id,
                object_id=object_id,
                tag_id=tag_id
            )
            if commit:
                tag_rel_instance.save()
            logger.info(f"Created TagRel instance with pk: {tag_rel_instance.pk}, tag_id: {tag_id}")
            instances.append(tag_rel_instance)
        return instances

    @staticmethod
    def _create_single_tag_rel(main_tag, content_type_id, object_id):
        instances = []
        if not TagRel.objects.filter(
            content_type_id=content_type_id,
            object_id=object_id,
            tag=main_tag
        ).exists():
            tag_rel_instance = TagRel(
                content_type_id=content_type_id,
                object_id=object_id,
                tag=main_tag
            )
            tag_rel_instance.save()
            logger.info(f"Created single TagRel instance with pk: {tag_rel_instance.pk}, tag: {main_tag}")
            instances.append(tag_rel_instance)
        return instances
