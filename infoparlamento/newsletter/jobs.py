from datetime import datetime

from django.db.models import FilteredRelation, Q
from django.utils import timezone
import logging
import re

from lxml import html
from lxml.html import clean

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.html import strip_tags

from django_rq import job

from infoparlamento.newsletter.models import Customer
from infoparlamento.utils import batch

logger = logging.getLogger(__name__)

OK = 0
WARNING = 1
ERROR = -1


def return_result(result, message):
    if logger:
        if result == OK:
            logger.info(message)
        elif result == WARNING:
            logger.warning(message)
        elif result == ERROR:
            logger.error(message)
        else:
            logger.debug(message)
    return {'exit_code': result, 'message': message}


def strip_tags_from_html_message(html_message: str) -> str:
    """Uses lxml to clean the html_message from tags,
    then strips all multiple spaces and backslashes, and return the text.

    Used to produce a textual representation of a notification email.

    :param html_message:
    :return: the stripped text
    """
    cleaner = clean.Cleaner()
    cleaner.javascript = True
    cleaner.style = True
    html_element = cleaner.clean_html(html.document_fromstring(html_message))
    html_string = html.tostring(html_element).decode()
    txt = strip_tags(html_string).strip()
    txt = re.sub(' +', ' ', txt)
    txt = re.sub('\n+', '\n', txt)
    txt = re.sub('\n +', '\n', txt)
    txt = re.sub('\n+', '\n', txt)
    txt = re.sub('&#160;', ' ', txt)
    txt = re.sub('&gt;', '>', txt)
    return txt


def send_mail(newsletter, preview, news, recipients):
    news = news.annotate(
        n=FilteredRelation(
            "category__newslettercategory",
            condition=Q(category__newslettercategory__newsletter=newsletter)
        )
    ).distinct().order_by('n__priority', 'n', 'priority')

    if 'https:' in settings.MEDIA_URL:
        img_url_prefix = ""
    else:
        img_url_prefix = settings.EMAIL_URL_PREFIX

    html_message = render_to_string(
        f'newsletter/email_template.html',
        context={
            'newsletter': newsletter,
            'preview': preview,
            'news': news,
            'img_url_prefix': img_url_prefix,
            'url_prefix': settings.EMAIL_URL_PREFIX,
        })

    text_message = strip_tags_from_html_message(html_message)

    for batch_recipients in batch(recipients, settings.EMAIL_BATCH_SIZE):
        email = EmailMultiAlternatives(
            newsletter.title,
            text_message,
            settings.DEFAULT_FROM_EMAIL,
            bcc=batch_recipients
        )
        email.attach_alternative(html_message, "text/html")
        email.send(fail_silently=False)

    return_result(OK, f"Mail sent to recipients for {', '.join(recipients)}.")


def send_single_news_mail(news, recipients):

    if 'https:' in settings.MEDIA_URL:
        img_url_prefix = ""
    else:
        img_url_prefix = settings.EMAIL_URL_PREFIX

    html_message = render_to_string(
        f'newsletter/email_single_news_template.html',
        context={
            'news': news,
            'img_url_prefix': img_url_prefix,
            'url_prefix': settings.EMAIL_URL_PREFIX
        })

    text_message = strip_tags_from_html_message(html_message)

    for batch_recipients in batch(recipients, settings.EMAIL_BATCH_SIZE):
        email = EmailMultiAlternatives(
            news.title,
            text_message,
            settings.DEFAULT_FROM_EMAIL,
            bcc=batch_recipients
        )
        email.attach_alternative(html_message, "text/html")
        email.send(fail_silently=False)

    return_result(OK, f"Mail sent to recipients for {', '.join(recipients)}.")

@job
def send_newsletter_emails(newsletter, preview=True, user=None):
    """Send newsletter to all recipients"""

    if preview:
        # send all news to the user
        recipients = [user.email]
        news = newsletter.news.all()
        send_mail(newsletter, preview, news, recipients)
    else:
        # send each customer a personalised mail
        result_msg = (f"Sending newsletter {newsletter.title} "
                      f"at {timezone.now().strftime('%Y-%m-%d %H:%M:%S')}\n")

        for customer in Customer.objects.filter(is_active=True):
            customer_news = newsletter.news.filter(customernews__customer=customer, customernews__is_excluded=False)
            if customer_news.count():
                recipients = [cr for cr in customer.recipients.values_list('email', flat=True)]
                send_mail(newsletter, preview, customer_news, recipients)
                result_msg += f" - {customer.name} ({customer_news.count()} news to {len(recipients)} recipients)\n"

        newsletter.sent_at = timezone.now()
        newsletter.save()
        newsletter.news.update(sent_at=timezone.now())

        return result_msg


@job
def send_single_news_to_all(news):
    """Send single news to all recipients of all customers"""
    news.sent_at = timezone.now()
    result_msg = (f"Sending single news {news.title} to all customers "
                  f"at {timezone.now().strftime('%Y-%m-%d %H:%M:%S')}\n")
    for customer in Customer.objects.filter(is_active=True):
        recipients = [cr for cr in customer.recipients.values_list('email', flat=True)]
        send_single_news_mail(news, recipients)
        result_msg += f" - {customer.name} ({len(recipients)})\n"

    news.save()

    return result_msg
