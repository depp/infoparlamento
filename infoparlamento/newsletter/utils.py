from infoparlamento.newsletter.models import News, NewsletterCategory, Category


def sync_newsletter_categories(nl):
    news_cat = News.objects.filter(is_ready=True, sent_at__isnull=True).values_list('category', flat=True).distinct()
    nl_cat = NewsletterCategory.objects.filter(newsletter=nl).values_list('category', flat=True)

    for cat_id in set(news_cat) - set(nl_cat):
        c = Category.objects.get(pk=cat_id)
        NewsletterCategory.objects.create(newsletter=nl, category=c, priority=c.priority)

    for cat_id in set(nl_cat) - set(news_cat):
        NewsletterCategory.objects.filter(newsletter=nl, category_id=cat_id).delete()
