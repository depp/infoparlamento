from django.urls import path

from infoparlamento.newsletter import views
from infoparlamento.newsletter.views import (
    news_detail_view, news_search_view, newsletter_detail_view, news_pdf_view, newsletter_list_view
)

app_name = "newsletter"
urlpatterns = [
    path("", view=newsletter_list_view, name="newsletter_list"),
    path("<int:pk>/", view=newsletter_detail_view, name="newsletter_detail"),
    path("news/<int:pk>/", view=news_detail_view, name="news_detail"),
    path('news/<int:pk>/pdf/', view=news_pdf_view, name='news_pdf'),
    path("ricerca/", view=news_search_view, name="news_search"),
    path('admin/get_subtags/', views.get_subtags, name='get_subtags'),
]
