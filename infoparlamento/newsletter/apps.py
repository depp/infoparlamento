from django.apps import AppConfig
from django.core.checks import register


class NewsletterConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "infoparlamento.newsletter"

    def ready(self):
        # Implicitly connect signal handlers decorated with @receiver.
        import infoparlamento.newsletter.signals

# Override the original check function to suppress the warning
def custom_check_ckeditor_version(app_configs, **kwargs):
    # Here we do not return anything, effectively suppressing the original warning
    return []

class CustomCKEditorConfig(AppConfig):
    default_auto_field = "django.db.models.AutoField"
    name = "ckeditor"

    def ready(self):
        # Register the custom check to suppress the warning
        register(custom_check_ckeditor_version)
