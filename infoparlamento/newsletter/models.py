import locale
import os

from auditlog.registry import auditlog
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.core.validators import EmailValidator, RegexValidator
from django.db import models

from ckeditor.fields import RichTextField
from django.db.models import Q
from django.utils import timezone

from infoparlamento.utils.behaviours import Prioritisable, Timestampable, GenericRelatable
from django.utils.translation import gettext_lazy as _


# Create your models here.

vat_validator = RegexValidator(r"\s{2}\d{11}", "Use country code: IT10662099154")
locale.setlocale(locale.LC_TIME, 'it_IT.UTF-8')

class Category(Prioritisable, models.Model):
    """
    A category to group news in a newsletter and form a table of content
    """

    class Meta:
        verbose_name = _("section")
        verbose_name_plural = _("sections")
        ordering = ["name"]

    name = models.CharField(
        max_length=64,
        verbose_name=_("Name"),
        help_text=_("Name of the section")
    )

    disabled = models.BooleanField(
        verbose_name=_("Disabled"),
        default=False,
        help_text=_("This section is disabled")
    )

    def __str__(self):
        return self.name


class TagRel(GenericRelatable, models.Model):
    """A relation between a generic object and a Tag
    """

    tag = models.ForeignKey(
        to="Tag",
        related_name="related_objects",
        verbose_name=_("Category"),
        help_text=_("Instance of category assigned to this object"),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return f"{self.content_object} - {self.tag}"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.update_news_tags_and_search_vector()

    def delete(self, *args, **kwargs):
        # store a reference to the related news before deletion
        related_news = self.content_object if isinstance(self.content_object, News) else None
        super().delete(*args, **kwargs)
        if related_news:
            related_news.update_tags_names_and_search_vector()

    def update_news_tags_and_search_vector(self):
        if isinstance(self.content_object, News):
            news_item = self.content_object
            news_item.update_tags_names_and_search_vector()

    def summary_text(self):
        # Generate the summary text here, using the same logic as the JavaScript function
        """
        Return a string that is a summary of the tag relation.
        """
        if self.tag.parent:
            return f"{self.tag.parent.denominazione} - {self.tag.denominazione}"
        else:
            return f"{self.tag.denominazione}"

    class Meta:
        verbose_name = _("Category relation")
        verbose_name_plural = _("Categories relations")


class Tag(models.Model):
    """
    A tag, that is, a thematic classification for a news or a customer
    """

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")
        ordering = ['denominazione']

    parent = models.ForeignKey(
        'self', null=True, blank=True,
        related_name='subtags',
        on_delete=models.CASCADE
    )

    denominazione = models.CharField(
        max_length=64,
        verbose_name=_("Name"),
        help_text=_("The name of the category")
    )

    disabled = models.BooleanField(
        verbose_name=_("Disabled"),
        default=False,
        help_text=_("This category is disabled")
    )

    def __str__(self) -> str:
        return f"{self.denominazione}"


class Newsletter(Timestampable, models.Model):
    """
    A newsletter is sent to all Customers on a given date
    """

    title = models.CharField(
        max_length=512,
        verbose_name=_("Title"),
        help_text=_("The title of the newsletter, used as object")
    )

    introductory_text = models.TextField(
        blank=True, null=True,
        verbose_name=_("Introductory text"),
        help_text=_("An introductory text, that is shown atop the news list within mails")
    )

    sent_at = models.DateTimeField(
        blank=True, null=True,
        verbose_name=_("Sent at"),
        help_text=_("The moment the newsletter was sent")
    )

    class Meta:
        verbose_name = _("Newsletter")
        verbose_name_plural = _("Newsletters")

    def __str__(self):
        if self.sent_at:
            formatted_date = timezone.localtime(self.sent_at).strftime("alle %H:%M del %d %b %Y")
            return f"{self.title} - Inviata {formatted_date}"
        else:
            return f"{self.title} - Non inviata"


class News(Prioritisable, Timestampable, models.Model):
    """
    News is the unit of information that compose a single newsletter
    """

    number = models.PositiveSmallIntegerField(
        verbose_name=_("Number"),
        blank=True, null=True, default=0,
        help_text=_("A sequence number")
    )
    title = models.TextField(
        max_length=512,
        verbose_name=_("Title"),
        help_text=_("A title for this news")
    )
    date = models.DateField(
        verbose_name=_("Date"),
        help_text=_("The date the news refer to")
    )
    text = RichTextField(
        verbose_name=_("Text"),
        config_name='text_ckeditor',
        help_text=_("The main content of the news")
    )
    author = models.ForeignKey(
        "users.User",
        on_delete=models.SET_NULL,
        blank=True, null=True,
        verbose_name=_("Author"),
        help_text=_("The author of the news")
    )
    is_ready = models.BooleanField(
        default=False,
        verbose_name=_("Is ready"),
        help_text=_("If the news is ready to be published")
    )
    is_ok_for_gianni = models.BooleanField(
        default=False,
        verbose_name=_("Is ok for Gianni"),
        help_text=_("If the news is marked ok by Gianni")
    )
    sent_at = models.DateTimeField(
        blank=True, null=True,
        verbose_name=_("Sent at"),
        help_text=_("The moment the newsletter was sent")
    )
    category = models.ForeignKey(
        "Category",
        on_delete=models.PROTECT,
        limit_choices_to={'disabled': False},
        verbose_name=_("Section"),
        related_name='news'
    )
    tags_rels = GenericRelation(
        to="TagRel", help_text=_("Categories related to this news")
    )
    tags_names = models.TextField(
        verbose_name=_("Tags Names"),
        help_text=_("Comma-separated list of tag names related to this news"),
        blank=True, default=""
    )
    search_vector = SearchVectorField(null=True)

    newsletter = models.ForeignKey(
        "Newsletter",
        on_delete=models.SET_NULL,
        verbose_name=_("Newsletter"),
        related_name='news',
        blank=True, null=True
    )
    __original_is_ready = False

    class Meta:
        verbose_name = _("News")
        verbose_name_plural = _("All news")
        # unique_together = ('category', 'number', )

    def __str__(self):
        if self.sent_at:
            formatted_date = timezone.localtime(self.sent_at).strftime("alle %H:%M del %d %b %Y")
            return f"{self.title} - Inviata {formatted_date}"
        else:
            return f"{self.title} - Non inviata"

    def compute_recipients(self):
        """Compute the automatic recipients of this news, based on tag's analysis"""
        recipients = Customer.objects.filter(
            Q(is_active=True) & (
                Q(tags_rels__tag__in=self.tags, tags_rels__tag__disabled=False) |
                Q(wants_it_all=True)
            )
        )
        return recipients.distinct()

    def compute_and_copy_recipients(self):
        """Compute the automatic recipients of this news, copy it into the CustomerNews table"""
        if self.pk:
            # first, reset, deleting all automatic, non-excluded recipients
            self.customernews_set.filter(is_excluded=False, is_manual=False).delete()

            # recompute recipients
            recipients = self.compute_recipients()

            # add recipients to the news with get_or_create
            for c in recipients:
                cn, created = self.customernews_set.get_or_create(customer=c, news=self)
                pass

    @property
    def tags(self):
        """Return the tags of the news, as a query set"""
        return Tag.objects.filter(id__in=self.tags_rels.values_list('tag', flat=True))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_is_ready = self.is_ready

    def update_tags_names_and_search_vector(self):
        # compute new tags_names
        tags_names = ", ".join(tag_rel.tag.denominazione for tag_rel in self.tags_rels.all())

        # compute new search_vector
        search_vector = (
            SearchVector('title', weight='A') +
            SearchVector('tags_names', weight='B') +
            SearchVector('text', weight='C')
        )

        # update fields without invoking save(), in order not to incur into infinite recursion
        News.objects.filter(pk=self.pk).update(
            tags_names=tags_names,
            search_vector=search_vector
        )

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        """Override save methond, intercepting the is_ready change"""

        # super(News, self).save(force_insert, force_update, *args, **kwargs)
        if self.is_ready != self.__original_is_ready:
            if self.is_ready:
                try:
                    nl = Newsletter.objects.get(sent_at__isnull=True)
                except Newsletter.DoesNotExist:
                    nl = None
                self.newsletter = nl
            else:
                self.newsletter = None
        self.__original_is_ready = self.is_ready
        self.compute_and_copy_recipients()
        super(News, self).save(force_insert, force_update, *args, **kwargs)


class NewsAttachmentFile(models.Model):
    """
    Documents or files attached to a single news
    """
    name = models.CharField(
        max_length=128,
        blank=True, null=True,
    )
    file = models.FileField(
        upload_to="files/%Y/%m/%d",
        verbose_name=_("File"),
    )
    news = models.ForeignKey(
        to=News,
        on_delete=models.CASCADE,
        related_name='files',
        verbose_name=_("News"),
    )

    def filename(self):
        return os.path.basename(self.file.name)

    class Meta:
        verbose_name = _("News Attachment File")
        verbose_name_plural = _("News Attachment Files")


class NewsletterCategory(Prioritisable, models.Model):
    """
    Category are prioritised differently for each newsletter.
    The default priorities come from the Category table.
    """
    newsletter = models.ForeignKey(
        "Newsletter",
        verbose_name=_("Newsletter"),
        on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        "Category",
        verbose_name=_("Section"),
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _("Newsletter section")
        verbose_name_plural = _("Newsletter sections")


class Customer(models.Model):
    """
    The customer receiving the mails
    """
    name = models.CharField(
        max_length=128,
        verbose_name=_("Name"),
        help_text=_("The denomination of the customer")
    )
    company_codes = models.CharField(
        max_length=256,
        blank=True, null=True,
        verbose_name=_("Company codes"),
        help_text=_("The VAT code or CF of the customer"),
    )
    eb_code = models.CharField(
        max_length=6,
        blank=True, null=True,
        verbose_name=_("Electronic Billing unique code"),
        help_text=_("The unique code of the customer for the electronic billing")
    )
    address = models.CharField(
        max_length=256,
        blank=True, null=True,
        verbose_name=_("Address"),
        help_text=_("The official address of the customer")
    )
    telephone = models.CharField(
        max_length=256,
        blank=True, null=True,
        verbose_name=_("Telephone numbers"),
        help_text=_("The official telephone numbers of the customer")
    )
    email = models.CharField(
        max_length=64,
        blank=True, null=True,
        verbose_name=_("Official email address"),
        help_text=_("The official email address of the customer"),
        validators=[EmailValidator]
    )
    contact_details = models.CharField(
        max_length=512,
        blank=True, null=True,
        verbose_name=_("Contacts"),
        help_text=_("Contact details for this customer")
    )
    contract_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_("Contract start_date"),
        help_text=_("The date when the contracts start")
    )
    contract_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_("Contract end_date"),
        help_text=_("The date when the contracts ends")
    )
    is_active = models.BooleanField(
        default=False,
        verbose_name=_("Is active"),
        help_text=_("If the customer is active")
    )
    wants_it_all = models.BooleanField(
        default=False,
        verbose_name=_("Wants it all"),
        help_text=_("If the customer wants all the news")
    )
    __original_wants_it_all = None
    tags_rels = GenericRelation("TagRel")

    news = models.ManyToManyField(
        "News",
        through="CustomerNews",
        related_name="customers"
    )

    class Meta:
        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")

    def __str__(self):
        if self.company_codes:
            return f"{self.name} - {self.company_codes}"
        else:
            return f"{self.name}"

    @property
    def tags(self):
        """Return the tags of the news, as a query set"""
        return Tag.objects.filter(id__in=self.tags_rels.values_list('tag', flat=True))

    def __init__(self, *args, **kwargs):
        super(Customer, self).__init__(*args, **kwargs)
        self.__original_wants_it_all = self.wants_it_all

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(Customer, self).save(force_insert, force_update, *args, **kwargs)
        if self.wants_it_all != self.__original_wants_it_all:
            if self.wants_it_all:
                for news in News.objects.filter(sent_at__isnull=True):
                    news.compute_and_copy_recipients()
            else:
                for news in self.news.filter(sent_at__isnull=True):
                    news.customernews_set.filter(customer=self, is_excluded=False).delete()
                    news.compute_and_copy_recipients()
        else:
            pass
        self.__original_wants_it_all = self.wants_it_all


class CustomerRecipient(models.Model):
    """Identifies a single recipient for a customer.
    A customer can have more than one recipient.
    """
    name = models.CharField(
        max_length=64,
        blank=True, null=True,
        verbose_name=_("Name"),
        help_text=_("The name of the recipient"),
    )
    email = models.CharField(
        max_length=64,
        verbose_name=_("Email address"),
        help_text=_("The email address of the recipient"),
        validators=[EmailValidator]
    )
    customer = models.ForeignKey(
        to="Customer",
        verbose_name=_("Customer"),
        on_delete=models.CASCADE,
        related_name="recipients",
    )

    def __str__(self) -> str:
        if self.name:
            return f"{self.email} ({self.name}"
        else:
            return self.email

    class Meta:
        verbose_name = _("Customer recipient")
        verbose_name_plural = _("Customer recipients")


class CustomerNews(models.Model):
    customer = models.ForeignKey(
        to="Customer",
        verbose_name=_("Customer"),
        on_delete=models.CASCADE
    )
    news = models.ForeignKey(
        to=News,
        verbose_name=_("News"),
        on_delete=models.CASCADE
    )
    is_excluded = models.BooleanField(
        default=False,
        verbose_name=_("Is excluded"),
        help_text=_("This record must be excluded forcedly")
    )
    is_manual = models.BooleanField(
        default=False,
        verbose_name=_("Is manual"),
        help_text=_("This recipient is added manually")
    )

    def __str__(self) -> str:
        return ""

    class Meta:
        verbose_name = _("Customer news")
        verbose_name_plural = _("Customer news")


auditlog.register(News, exclude_fields=['created_at', 'updated_at'])
