from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from django.utils.encoding import force_str

import mistune

register = template.Library()


@register.tag(name="markdown")
def markdown_tag(parser, token):
    nodelist = parser.parse(("endmarkdown",))
    parser.delete_first_token()  # consume '{% endmarkdown %}'
    return MarkdownNode(nodelist)


class MarkdownNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        value = self.nodelist.render(context)
        markdown = mistune.create_markdown(
            escape=False,
            renderer='html',
            plugins=['url', 'strikethrough', 'footnotes', 'table'],
        )
        try:
            return mark_safe(markdown(value))
        except ImportError:
            if settings.DEBUG:
                raise template.TemplateSyntaxError(
                    "Error in `markdown` tag: " "The mistune library isn't installed."
                )
            return force_str(value)
