from html import unescape

from django import template

register = template.Library()


@register.filter
def unescaped(html_escaped_text):
    return unescape(html_escaped_text)
