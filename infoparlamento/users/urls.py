from django.urls import path

from infoparlamento.users.views import (
    user_detail_view,
    user_redirect_view,
    user_update_view, impersonate_user, stop_impersonation,
)

app_name = "users"
urlpatterns = [
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    path('impersonate/<int:user_id>/', view=impersonate_user, name='impersonate-user'),
    path('stopimpersonation/', view=stop_impersonation, name='stop-impersonation'),
    path("<str:username>/", view=user_detail_view, name="detail"),
]
