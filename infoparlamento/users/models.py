from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, ForeignKey, CASCADE, OneToOneField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from infoparlamento.newsletter.models import Customer


class User(AbstractUser):
    """
    Default custom user model for InfoParlamento.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """

    # First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)
    first_name = None  # type: ignore
    last_name = None  # type: ignore

    customer = OneToOneField(
        Customer,
        null=True, blank=True,
        on_delete=CASCADE,
        related_name='user',
        unique=True
    )

    def get_absolute_url(self) -> str:
        """Get URL for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})
