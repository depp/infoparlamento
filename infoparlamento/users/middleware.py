from infoparlamento.users.models import User


class UserImpersonationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        impersonate_id = request.session.get('impersonate_id')

        if request.user.is_authenticated and impersonate_id:
            try:
                request.impersonated_user = User.objects.get(id=impersonate_id)
                request.real_user = request.user
                request.user = request.impersonated_user
            except User.DoesNotExist:
                request.session.pop('impersonate_id', None)

        return self.get_response(request)
