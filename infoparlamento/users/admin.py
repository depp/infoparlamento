from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from infoparlamento.users.forms import UserAdminChangeForm, UserAdminCreationForm

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm
    fieldsets = (
        (None, {"fields": ("username", "password", "customer")}),
        (_("Personal info"), {"fields": ("name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    list_display = ["username", "name", "is_superuser", "view_groups"]
    search_fields = ["name", "username", "email", ]
    raw_id_fields = ("customer", )

    def get_queryset(self, request):
        return (super(UserAdmin, self).
            get_queryset(request)
                .select_related('customer')
                .prefetch_related('groups')
        )

    @staticmethod
    @admin.display(description='gruppi')
    def view_groups(obj):
        return ", ".join(group.name for group in obj.groups.all())
