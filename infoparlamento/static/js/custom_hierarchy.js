document.addEventListener('DOMContentLoaded', function() {
    function attachMainTagChangeListener(mainTagSelect) {
        mainTagSelect.addEventListener('change', function() {
            var mainTagId = this.value;
            var row = this.closest('tr');  // Find the closest table row
            var subTagSelectOld = row.querySelector('.selector-available select[name$="sub_tags_old"]');
            var subTagSelectChosen = row.querySelector('.selector-chosen select[name$="sub_tags"]');

            // Clear both sub-tag dropdowns
            SelectBox.move_all(subTagSelectChosen.id, subTagSelectOld.id);
            subTagSelectOld.innerHTML = '';
            subTagSelectChosen.innerHTML = '';

            if (mainTagId) {
                // Send an AJAX request to fetch the sub-tags
                fetch(window.location.origin + `/newsletters/admin/get_subtags/?main_tag_id=${mainTagId}`)
                    .then(function(response) {
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        return response.json();
                    })
                    .then(function(data) {
                        // Add the new sub-tag options
                        data.subtags.forEach(function(subtag) {
                            var option = document.createElement('option');
                            option.value = subtag.id;
                            option.text = subtag.denominazione;
                            subTagSelectOld.appendChild(option);
                        });

                        // Reinitialize the SelectBox to attach the new options
                        SelectBox.init(subTagSelectOld.id);
                        SelectBox.init(subTagSelectChosen.id);

                        // Refresh the UI elements
                        SelectFilter.refresh_icons(subTagSelectChosen.id.replace('_to', ''));

                    })
                    .catch(function(error) {
                        console.error('Error fetching sub-tags:', error);
                    });
            }
        });
    }

    // Attach event listener to all initial main_tag selects
    document.querySelectorAll('select[name$="main_tag"]').forEach(function(mainTagSelect) {
        attachMainTagChangeListener(mainTagSelect);
    });

    // Listen for new inlines being added and attach event listener
    document.body.addEventListener('formset:added', function(event) {
        var mainTagSelect = event.target.querySelector('select[name$="main_tag"]');
        if (mainTagSelect) {
            attachMainTagChangeListener(mainTagSelect);
        }
    });
});
